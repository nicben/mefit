import {Box, CircularProgress} from "@material-ui/core";
import React from "react";
import {Redirect, withRouter} from "react-router-dom";
import {connect} from "react-redux";

const LoadingComponent = (props) => {
    const tokenKey = localStorage.getItem("meFitLoginToken");
    if(!props.auth && tokenKey === null){
        return <Redirect to='/' />
    }
    const isLoading = () => {
        if(props.loading){
            return(
                <Box
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    minHeight="100vh"
                >

                    <CircularProgress />
                </Box>

            )
        }
        else return "";
    }
    return(
        <>
            {isLoading()}
        </>

    )

}
const mapStateToProps = (state) => {
    return {
        loading: state.auth.loading,
    };
}


export default withRouter(connect(mapStateToProps)(LoadingComponent));