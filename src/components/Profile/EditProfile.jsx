import {useDispatch, useSelector} from "react-redux";
import {useState} from "react";
import {createAccount} from "../../store/actions/accountActions";
import {Button} from "react-bootstrap";
import * as React from "react";
import {Box, TextField} from "@material-ui/core";

const EditProfile = handleSave => {
    const auth = useSelector(state => state.auth)
    const {firstName, lastName, height, weight, email, contributor, imageUrl} = useSelector(state => state.auth)
    const [inputFirstname, setFirstname] = useState(firstName);
    const [inputLastname, setLastname] = useState(lastName);
    const [inputHeight, setHeight] = useState(height);
    const [inputWeight, setWeight] = useState(weight);
    const [inputEmail, setEmail] = useState(email);
    const [contributorStatus, setContributorStatus] = useState(contributor);
    const [inProcess, setInProcess] = useState(true);

    const dispatch = useDispatch()

    function handleFirstnameChange(event) {
        setFirstname(event.target.value);
        console.log(event.target.value);
    }

    function handleLastnameChange(event) {
        setLastname(event.target.value);
        console.log(event.target.value);
    }

    function handleEmailChange(event) {
        setEmail(event.target.value);
        console.log(event.target.value);
    }

    function handleHeightChange(event) {
        setHeight(event.target.value);
        console.log(event.target.value);
    }

    function handleWeightChange(event) {
        setWeight(event.target.value);
        console.log(event.target.value);
    }

    function handleContributorChange() {
        setContributorStatus(2)
        console.log("send request clicked ");
    }

    const handleCreation = (event) => {
        handleSave()
        setInProcess(false)
        console.log("submit clicked");
        event.preventDefault();
        dispatch(createAccount({
            inputFirstname,
            inputLastname,
            inputHeight,
            inputWeight,
            inputEmail,
            contributorStatus
        }))
        console.log("Profile page state " + inputFirstname + " " +
            inputLastname + " " +
            inputHeight + " " +
            inputWeight + " " +
            inputEmail + " " +
            contributorStatus)
    }

        const cancelCreation = () => {
            setInProcess(false)
            console.log("Cancel clicked");
        }

        function handleEditClicked() {
            setInProcess(false)
            console.log("edit clicked");
        }

        return (
            <>
                <br/>
                <form onSubmit={handleCreation}>
                    <label>Firstname:&nbsp;&nbsp;&nbsp;&nbsp; </label>
                    <input className="fancyInput" type="text" placeholder={firstName} onChange={handleFirstnameChange}/>
                    <br/>

                    <label>Lastname:&nbsp;&nbsp;&nbsp;&nbsp; </label>
                    <input className="fancyInput" type="text" placeholder={lastName} onChange={handleLastnameChange}/>
                    <br/>
                    <label>Email:&nbsp;&nbsp;&nbsp;&nbsp; </label>
                    <input className="fancyInput" type="text" placeholder={email} onChange={handleEmailChange}/>
                    <br/>
                    <label>Height:&nbsp;&nbsp;&nbsp;&nbsp; </label>
                    <input className="fancyInput" type="number" placeholder={height} onChange={handleHeightChange}/>
                    <br/>
                    <label>Weight:&nbsp;&nbsp;&nbsp;&nbsp; </label>
                    <input className="fancyInput" type="number" placeholder={weight} onChange={handleWeightChange}/>
                    <br/>
                    {(contributor === 1 || contributor === 0) &&
                    <label>
                        CONTRIBUTOR:
                        <input type="checkbox" onChange={handleContributorChange}/>
                    </label>
                    }
                    <br/><br/>
                    <input className="custom-button" style={{
                        paddingLeft: "1rem",
                        paddingRight: "1rem",
                        paddingTop: "0.5rem",
                        paddingBottom: "0.5rem",
                        marginBottom: "0.5rem",
                        marginRight: "0.5rem"
                    }} type="submit" value="Save Changes"/>
                    <Button className="custom-button" type="cancel">Cancel</Button>
                </form>
            </>
        )
    }

    export default EditProfile
