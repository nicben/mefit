import {withRouter} from "react-router-dom";
import {connect, useDispatch} from "react-redux";
import * as React from "react";
import {useState} from "react";
import {
    MenuItem,
    Select,
    TextField,
    TableHead,
    TableCell,
    TableRow,
    TableBody,
    TableContainer
} from "@mui/material";
import {createWorkout} from "../../store/actions/overviewActions";
import {Button} from "react-bootstrap";

const AddWorkoutComponent = (props) => {
    const [chosenExcerxises, setchosenExcerxises] = useState([])
    const [finsihedSets,setFinsihedSets] = useState([])
    const [changed,setChanged] = useState(false)
    const [add, setAdd] = useState(false)
    const dispatch = useDispatch();
    const listOptions = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
    const [selectedWorkoutType,setSelectedWorkoutType] = useState("none")
    const [workoutName, setWorkoutName] = useState("");

    const createWorkoutClick = () => {
        let list = []
        for (let i = 0; i < finsihedSets.length; i++) {
            const set = finsihedSets[i]

            list.push({exerciseId:set.id,reps:set.reps,sets:set.sets})
        }

        dispatch(createWorkout([{madeByContributor:true,difficulty: 0,title:workoutName,type:selectedWorkoutType,sets:list}]))
        setAdd(false);
    }

    const addExcercise = (id,name) => {
        setChanged(true)
        if(changed){
            setChanged(false)
            setChanged(true)
        }
        let exist = false;
        let index = 0;
        for (let i = 0; i < chosenExcerxises.length; i++) {
            if(chosenExcerxises[i].id === id){
                exist = true;
                index = i;
                break;
            }
        }
        if(exist){
            let set = chosenExcerxises[index]
            if(set.sets === undefined) set["sets"] = 1;
            if(set.reps === undefined) set["reps"] = 1;

            setFinsihedSets([...finsihedSets,set])
        }
        else{
            let set = {name:name,sets:1,reps:1,id:id}

            setFinsihedSets([...finsihedSets,set])
        }
    }

    const addSetsToExercise = (id,number,name) => {
        let list = chosenExcerxises;
        let index = -1;
        let idExists = false;
        for (let i = 0; i < list.length; i++) {
            if(id===list[i].id){
                idExists = true;
                index = i;
                break;
            }
        }
        if(idExists){
            list[index]["reps"] = number;
        }
        else{
            list.push({id:id,"reps":number,name:name})
        }
        setchosenExcerxises(list)
    }

    const addRepsToExercise = (id,number,name) => {
        let list = chosenExcerxises;
        let index = -1;
        let idExists = false;
        for (let i = 0; i < list.length; i++) {
            if(id===list[i].id){
                idExists = true;
                index = i;
                break;
            }
        }
        if(idExists){
            list[index].sets = number;
        }
        else{
            list.push({id:id,"sets":number,name:name})
        }
        setchosenExcerxises(list)
    }

    const renderchosenWorkouts = () => {
        return(
            <TableBody >
                {finsihedSets.map((row) => (
                    <TableRow key={row.id}>
                        <TableCell component="th" scope="row">
                            {row.name}
                        </TableCell>
                        <TableCell>{row.sets}</TableCell>
                        <TableCell>{row.reps}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
        )
    }

    const render = ( ) => {
        if(changed){
            return(<>
                    <TextField id="standard-basic" label="Title of New Workout" variant="standard" onChange={(e) => setWorkoutName(e.target.value)} />
                    <br/><br/>
                    <TableContainer>
                        <table>
                            <TableHead>
                                <TableRow style={{cursor:'pointer'}}>
                                    <TableCell >Exercise</TableCell>
                                    <TableCell >Choose sets</TableCell>
                                    <TableCell >Choose reps</TableCell>
                                </TableRow>
                            </TableHead>
                            {renderchosenWorkouts()}
                        </table>
                    </TableContainer>
                    <br/>
                    <select onChange={(e)=>setSelectedWorkoutType(e.target.value)} name="category" id="categories">
                        <option value="strength">Strength</option>
                        <option value="balance">Balance</option>
                        <option value="flexibility">Flexibility</option>
                        <option value="mobility">Mobility</option>
                        <option value="coordination">Coordination</option>
                        <option value="agility">Agility</option>
                    </select>
                    <br/><br/>
                    <Button className="custom-button" onClick={createWorkoutClick}>Create workout</Button>
                    <Button className="cancel-button" onClick={() => setAdd(false)}>Cancel</Button>
                </>
            )
        }
        else return (
            ""
        )
    }
    const chosenExcercises = () =>{
        let list = props.exercises.exercises;
        if(list===undefined)return "";
        let newList = [];
        for (let i = 0; i<list.length;i++){
            let add = true;
            for(let j = 0; j<chosenExcerxises.length; j++){
                if(list[i].id === chosenExcerxises[j].id){
                    add = false;
                }
            }
            if(add){
                newList.push(list[i])
            }
        }
        return (newList.map((field)  => (
            <>
                <TableBody>
                    <TableRow key={field.id}>
                        <TableCell component="th" scope="row">
                            {field.name}
                        </TableCell>
                        <TableCell><Select defaultValue={1} onChange={e => addRepsToExercise(field.id, e.target.value,field.name)}>
                            {listOptions.map((number) =>
                                <MenuItem key={number} value={number}>{number}</MenuItem>
                            )}
                        </Select></TableCell>
                        <TableCell><Select defaultValue={1} onChange={e => addSetsToExercise(field.id, e.target.value,field.name)}>
                            {listOptions.map((number) =>
                                <MenuItem key={number} value={number}>{number}</MenuItem>
                            )}
                        </Select></TableCell>
                        <Button className="custom-button" onClick={()=>addExcercise(field.id,field.name)}>Add</Button>
                    </TableRow>
                </TableBody>
            </>
        )))
    }
    const renderBase = () => {
        if(add){
            return(
                <>
                    {render(finsihedSets)}
                    <h2>Exercises to choose from</h2>
                    <TableHead>
                        <TableRow style={{cursor:'pointer'}}>
                            <TableCell >Exercise</TableCell>
                            <TableCell >Choose sets</TableCell>
                            <TableCell >Choose reps</TableCell>
                        </TableRow>
                    </TableHead>
                    {chosenExcercises()}
                    <Button className="cancel-button" onClick={()=>{setAdd(false)}}>Cancel</Button>
                </>
            )
        }
        else if(props.user.contributor === 3 || props.user.admin === 3){
            return (
                <Button className="custom-button" onClick={() => setAdd(true)}>Add Workout</Button>
            )
        }
        else return "";
    }

    return(
        <>
            {renderBase()}
        </>
    )
}
const mapStateToProps = (state) => {
    return {
        exercises: state.exercises,
        user : state.auth
    };
}

export default withRouter(connect(mapStateToProps)(AddWorkoutComponent));