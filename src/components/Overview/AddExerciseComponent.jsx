import {useState} from "react";
import {Checkbox, FormControlLabel, TextField} from "@material-ui/core";
import {Box} from "@mui/material";
import {connect, useDispatch} from "react-redux";
import {addExercise} from "../../store/actions/overviewActions";
import {withRouter} from "react-router-dom";
import {Button} from "react-bootstrap";


const AddExerciseComponent = (props)=>{
    const dispatch = useDispatch()
    const [addExercises, setAddExercises] = useState(false);
    const [exerciseDescription, setDescription] = useState("")
    const [exerciseName, setExerciseName] = useState("")
    const [exerciseUrl, setUrl] = useState("")
    const [selectedMuscles, setSelectedMuscles] = useState( {Biceps : false,
        Triceps : false,
        Back : false,
        Legs : false,
        Calves : false,
        Chest : false,
        Glutes : false,
        Shoulders : false});
    //Methods to set input values
    const changeDescription = (event) =>{
        setDescription(event.target.value)
    }
    const changeUrl = (event) =>{
        setUrl(event.target.value)
    }
    const changeExerciseName = (event) =>{
        setExerciseName(event.target.value)
    }

    //helps to change what is
    const onclickaddExercises = () => {
        if(addExercises) setAddExercises(false);
        else setAddExercises(true);
    }
    const addExerciseTodb = () => {
        let muscle = "";
        for(const key in selectedMuscles){
            if(selectedMuscles[key]) muscle += ", " + key
        }
        muscle = muscle.substring(2);
        const exericseObjec = {
            name: exerciseName,
            videoUrl: exerciseUrl,
            description: exerciseDescription,
            muscules: muscle

        }
        dispatch(addExercise({exericseObjec, exercises: props.exercises.exercises, sessionToken : props.auth}));

    }
    const test = (muscle) => {
        selectedMuscles[muscle] = !selectedMuscles[muscle];
        setSelectedMuscles(selectedMuscles)
    }

    const conditionalRender = () =>{
        if(addExercises){
            return (
                <div>
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': { m: 1, width: '25ch' },
                        }}
                        noValidate
                        autoComplete="off"
                    >
                        <TextField id="standard-basic" label="Exercise" variant="standard" onChange={changeExerciseName} />
                        <TextField
                            id="outlined-textarea"
                            label="Description"
                            placeholder="Placeholder"
                            multiline
                            onChange={changeDescription}
                        />
                        <TextField id="standard-basic" label="videoURL" variant="standard" onChange={changeUrl}/>
                    </Box>
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': { m: 1, width: '15h' },
                        }}
                        noValidate
                        autoComplete="off"
                    >
                        <FormControlLabel  control={<Checkbox color="var(--button-color)" onChange={()=>test("Chest")} />} label="Chest" />
                        <FormControlLabel  control={<Checkbox color="var(--button-color)" onChange={()=>test("Triceps")}   />} label="Triceps" />
                        <FormControlLabel  control={<Checkbox color="var(--button-color)" onChange={()=>test("Biceps")}  />} label="Biceps" />
                        <FormControlLabel  control={<Checkbox color="var(--button-color)" onChange={()=>test("Back")}   />} label="Back" />
                    </Box>
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': { m: 1, width: '15h' },
                        }}
                        noValidate
                        autoComplete="off"
                    >
                        <FormControlLabel control={<Checkbox color="var(--button-color)" onChange={()=>test("Legs")} />} label="Legs" />
                        <FormControlLabel control={<Checkbox color="var(--button-color)" onChange={()=>test("Shoulders")} />} label="Shoulders" />
                        <FormControlLabel control={<Checkbox color="var(--button-color)" onChange={()=>test("Calves")} />} label="Calves" />
                        <FormControlLabel control={<Checkbox color="var(--button-color)" onChange={()=>test("Glutes")} />} label="Glutes" />
                    </Box>
                    <Button className="custom-button" onClick={addExerciseTodb}>Add exercise</Button>
                    <Button className="cancel-button" onClick={onclickaddExercises}>Cancel</Button>
                </div>
            );
        }
        else if(props.user.contributor === 3 || props.user.admin === 3){
            return <Button className="custom-button" onClick={onclickaddExercises}>Add exercises</Button>
        }
        else{
            return "";
        }
    }

    return(
        <div >

            {conditionalRender()}
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth.userToken,
        exercises: state.exercises,
        user: state.auth
    };
}

export default  withRouter(connect(mapStateToProps)(AddExerciseComponent));