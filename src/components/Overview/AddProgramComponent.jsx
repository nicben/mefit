import {withRouter} from "react-router-dom";
import {connect, useDispatch} from "react-redux";
import {TableHead, TableRow, TableCell, TableBody, Modal, Box, Paper, Table, TextField} from "@mui/material";
import * as React from "react";
import {useState} from "react";
import TableContainer from "@material-ui/core/TableContainer";
import {createNewProgram} from "../../store/actions/programActions";
import {Button} from "react-bootstrap";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const AddProgramComponent = (props) => {
    const [addProgram,setAddProgram] = useState(false);
    const [selectedWorkouts,setSelectedPrograms] = useState([])
    const [open, setOpen] = React.useState(false);
    const [displayWorkoutId,setDisplayWorkoutId] = useState(0);

    const [programName, setProgramName]= useState("");
    const [programType, setProgramType] = useState("");

    const dispatch = useDispatch();
    const workoutClicked = (id) =>{
        setDisplayWorkoutId(id)
        handleOpen()
    }

    const createProgram = () => {
        let listWithId = [ ]
        for (let i = 0; i <selectedWorkouts.length ; i++) {
            listWithId.push(selectedWorkouts[i].id)
        }
        dispatch(createNewProgram({title:programName,category:programType,workouts:listWithId}))
    }

    const removeWorkout = (id) => {
        setSelectedPrograms(selectedWorkouts.filter((element) => element.id !== id));
    }

    const displaySelectedWorkouts = ()=> {
        if(selectedWorkouts.length===0) return "";
        if(!addProgram) return ""

        let body = (
            <>
            <TableBody >
                {selectedWorkouts.map((row) => (
                    <TableRow key={row.id}>
                        <TableCell component="th" scope="row">
                           {row.title}
                        </TableCell>
                        <TableCell>
                            <Button className="custom-button" onClick={()=>workoutClicked(row.id)}>View details</Button>
                            <Button className="cancel-button" onClick={()=>removeWorkout(row.id)}>Delete</Button>
                        </TableCell>
                    </TableRow>
                ))}

            </TableBody>
                <br/><br/>
                <select onChange={(e) => setProgramType(e.target.value)} name="category" id="categories">
                    <option value="strength">Strength</option>
                    <option value="balance">Balance</option>
                    <option value="flexibility">Flexibility</option>
                    <option value="mobility">Mobility</option>
                    <option value="coordination">Coordination</option>
                    <option value="agility">Agility</option>
                </select>
                <br/>
                <br/>
                <Button className="custom-button" onClick={()=>{createProgram()}}>Create Program</Button>
                <Button className="cancel-button" onClick={()=>setAddProgram(false)}>Cancel</Button>

                </>
        )

        return(
            <>
                <TextField id="standard-basic" label="Program Title" variant="standard" onChange={(e) => setProgramName(e.target.value)} />
                <br/><br/>
                <h3>Workouts added to this program:</h3>
                {renderHead()}
                {body}
            </>
        )
    }

    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const changeAddProgramState = (bool) => {
        setAddProgram(bool)
    }

    const addWorkout = (id,title) =>{
        setSelectedPrograms([...selectedWorkouts, {id:id, title:title}])
    }

    const workouts = () =>{
        return(
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 300 }} wrapperStyle={{ maxHeight: 100 }}>
                    <TableHead align={"center"}>
                        <TableRow style={{cursor:'pointer'}}  align={"center"} >
                            <TableCell>Exercise</TableCell>
                            <TableCell>Reps</TableCell>
                            <TableCell>Sets</TableCell>
                        </TableRow>
                    </TableHead>
                    {sets()}
                </Table>
            </TableContainer>
        )
    }

    const sets = () => {
        let setId = []
        for (const workout of props.workouts) {
            if(workout.id===displayWorkoutId){
                for(const id of workout.sets){
                    setId.push(id);
                }
            }
        }

        let row = []
        for(const id of setId){
            for (const set of props.sets){
                if(id === set.id){
                    row.push({exercise: set.exercise.name, sets: set.sets, reps: set.reps})
                }
            }
        }

        return(
            <TableBody >
                {row.map((row) => (
                    <TableRow key={row.id}>
                        <TableCell component="th" scope="row">
                            {row.exercise}
                        </TableCell>
                        <TableCell>{row.sets}</TableCell>
                        <TableCell>{row.reps}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
        )
    }

    const renderBody = () =>{
        let workouts = []
        for (let i = 0; i < props.workouts.length; i++) {
            let add = true;
            for (let j = 0; j < selectedWorkouts.length; j++) {
                if(props.workouts[i].id === selectedWorkouts[j].id) add = false;
            }
            if(add) workouts.push(props.workouts[i])
        }

        workouts = workouts.filter((workout) =>
            workout.madeByContributor===true
        )
        return(
            <TableBody >
                {workouts.map((row) => (
                    <TableRow key={row.id}>
                        <TableCell component="th" scope="row">
                            {row.title}
                        </TableCell>
                        <TableCell>
                            <Button className="custom-button" onClick={()=>workoutClicked(row.id)}>View details</Button>
                            <Button className="custom-button" onClick={()=> {addWorkout(row.id, row.title);}}>Add</Button>
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        )
    }

    const renderHead = () => {

        return(
            <TableHead>
                <TableRow style={{cursor:'pointer'}}>
                    <TableCell>Workouts</TableCell>
                </TableRow>
            </TableHead>
        )

    }

    const tableRender = () => {
        if(addProgram){
            return(
                <><br/><br/>
                    <h3>Select workouts to program</h3>
                    <TableContainer>
                        <table>
                            {renderHead()}
                            {renderBody()}
                        </table>
                    </TableContainer>
                </>
            )
        }
        return ""
    }

    const renderButtons = () => {
        if(addProgram){
            return(
                <>
                    <Button className="cancel-button" onClick={()=>changeAddProgramState(false)}>Cancel</Button>
                    <br/><br/>
                </>
            )
        }
        else {
            return (<Button style={{float:"top right"}} className="custom-button" onClick={()=>changeAddProgramState(true)}>Add program</Button>)
        }
    }
    const condition = () =>{
        if(props.auth.contributor !== 3 && props.auth.admin!==3) return "";

        return(
            <>
                {displaySelectedWorkouts()}
                {tableRender()}
                {renderButtons()}
                <div>
                    <Modal
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={style}>
                            {workouts()}
                        </Box>
                    </Modal>
                </div>

            </>

        )
    }
    return(
        <>
            {condition()}
        </>
    );
}

const mapStateToProps = (state) =>{
    return{

        workouts: state.workouts.workouts,
        auth: state.auth,
        exercises: state.exercises,
        sets : state.overview.sets
    };
}

export default withRouter(connect(mapStateToProps)(AddProgramComponent));