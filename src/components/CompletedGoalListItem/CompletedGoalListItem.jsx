import {connect, useSelector} from "react-redux";
import {useState} from "react";
import {
    Box,
    Modal,
    Tab,
    Paper,
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody
} from "@mui/material";
import * as React from "react";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import {withRouter} from "react-router-dom";
import {Button} from "react-bootstrap";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 900,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const CompletedGoalListItem = (props) => {
    const {completedGoals} = useSelector(state => state.goals)
    //const [completedGoalsList, setCompletedGoalsList] = useState(completedGoals)
    const [goalClickedId, setGoalClickedID] = useState(0)
    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () =>{

        setValue(1)
        setOpen(false)
    };

    const [value, setValue] = useState(1);
    const handleChange = (event, newValue) => {
        console.log("NEW VALUE " + newValue)
        setValue(newValue);
    };

    const sets = (displayWorkoutId) => {
        let setId = []
        for (const workout of props.workouts) {
            if(workout.id===displayWorkoutId){
                for(const id of workout.sets){
                    setId.push(id);
                }
            }
        }
        let row = []
        for(const id of setId){
            for (const set of props.sets){
                if(id === set.id){
                    row.push({exercise: set.exercise.name, sets: set.sets, reps: set.reps})
                }
            }
        }
        return(
            <TableBody >
                {row.map((row) => (
                    <TableRow key={row.id}>
                        <TableCell component="th" scope="row">
                            {row.exercise}
                        </TableCell>
                        <TableCell>{row.sets}</TableCell>
                        <TableCell>{row.reps}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
        )
    }


    const viewProgramInfo = () => {
        let selectedGoal = [];
        for (let i = 0; i<completedGoals.length;i++){
            if(completedGoals[i].id == goalClickedId){
                selectedGoal = completedGoals[i];
            }
        }

        let workouts = selectedGoal.workouts;

        if(goalClickedId!== 0){
            let day = 1
            return (workouts.map((field) => (
                <TabPanel value={day++} >
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 300 }} wrapperStyle={{ maxHeight: 100 }}>
                            <TableHead align={"center"}>
                                <TableRow style={{cursor:'pointer'}}  align={"center"} >
                                    <TableCell>Exercise</TableCell>
                                    <TableCell>Reps</TableCell>
                                    <TableCell>Sets</TableCell>
                                </TableRow>
                            </TableHead>
                            {sets(field)}
                        </Table>
                    </TableContainer>
                </TabPanel>
            )))}
    }

    const selectedProgramView = () => {
        let day = 0
        let selectedGoal = [];
        for (let i = 0; i<completedGoals.length;i++){
            if(completedGoals[i].id == goalClickedId){
                selectedGoal = completedGoals[i];
            }
        }
        let workouts = selectedGoal.workouts;


        if(goalClickedId !== 0){return (
            workouts.map((field) => (
                <Tab label={"Workout " + ++day} value={day}/>
            )))}
        return ""
    }


    const renderGoalDetails = () =>{
        return(
        <>
            <Box sx={{ width: '100%', typography: 'body1' }}>
                <TabContext value={value}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <TabList onChange={handleChange} aria-label="lab API tabs example">
                            {selectedProgramView()}
                        </TabList>
                    </Box>
                    {viewProgramInfo()}
                </TabContext>
            </Box>
        </>)
    }

    function WithoutTime(dateTime) {
        return dateTime.substring(0, 10);
    }

    return (<>
        {completedGoals.map((goal) =>
            goal.completed === true &&
            <tr key={goal.id} className="coloredRows">
                <td className="rowText">{goal.title}</td>
                <td className="rowText">{WithoutTime(goal.startDate)}</td>
                <td className="rowText">{goal.category}</td>
                <td className="rowText"><Button
                    className="custom-button" style={{backgroundColor: "var(--button-color)", float:"right", marginRight:"2rem", color: "white"}}
                    onClick={() => {
                    setGoalClickedID(goal.id)
                    handleOpen();
                }}>View more details</Button></td>
            </tr>
        )}
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                {renderGoalDetails()}
            </Box>
        </Modal>
    </> );
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        exercises: state.exercises,
        workouts: state.workouts.workouts,
        sets : state.overview.sets,
        programs: state.programs.programs
    };
}


export default withRouter(connect(mapStateToProps)(CompletedGoalListItem));