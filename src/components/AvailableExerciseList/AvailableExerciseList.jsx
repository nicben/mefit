import Table from "@material-ui/core/Table";
import AvailableExerciseListItem from "../AvailableExerciseListItem/AvailableExerciseListItem";

const AvailableExerciseList = (props) => {
    const { tabIndex } = props;

    return (
        <Table>
            <tbody>
                <tr>
                    <th>Title</th>
                    <th>Add exercise</th>
                </tr>
                <AvailableExerciseListItem tabIndex={tabIndex}/>
            </tbody>
        </Table>
    );
}
export default AvailableExerciseList;