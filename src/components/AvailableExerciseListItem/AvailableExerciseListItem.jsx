import {Button} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {userExerciseActionSelected, userWorkoutActionAddExercise} from "../../store/actions/userGoalsActions";

const AvailableExerciseListItem = (props) => {
    const {tabIndex} = props;
    const {exercises} = useSelector(state => state.exercises);
    const {selectedExercises, program} = useSelector(state => state.userGoal);
    const dispatch = useDispatch();


    const addExerciseClicked = (exercise) => {
        let currentTab = program.filter(workout => workout.tabId === tabIndex)
        if(currentTab.length){
            dispatch(userWorkoutActionAddExercise(selectedExercises, tabIndex))
            dispatch(userExerciseActionSelected(exercise))
        }
        else {
            dispatch(userExerciseActionSelected(exercise))
        }
        return exercises.filter(exercise => exercise.id === tabIndex)
    }
    return exercises.map(exercise =>
        <tr key={exercise.id}>
            <td>{exercise.name}</td>
            <td><Button variant="outlined" onClick={() => addExerciseClicked(exercise)}>Add</Button></td>
        </tr>
    );
}
export default AvailableExerciseListItem;