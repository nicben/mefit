import {
    Dialog,
    Button,
    styled
} from "@mui/material";
import {useState} from "react";
import TabPanel from "../TabPanel/TabPanel"
import {goalActionDelete} from "../../store/actions/goalActions";
import {userGoalActionResetModalInput} from "../../store/actions/userGoalsActions";
import {useDispatch, useSelector} from "react-redux";

const StyledDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

const Modal = (props) => {
    const {handleAddClicked, disabled, className} = props;
    const [open, setOpen] = useState(false);
    const {goalId} = useSelector(state => state.goals)
    const dispatch = useDispatch();

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const handleModalReset = () => {
        dispatch(goalActionDelete(goalId))
        dispatch(userGoalActionResetModalInput())
    }

    return (
        <div>
            <Button disabled={disabled} className={className} variant="outlined" onClick={() => { handleAddClicked();handleClickOpen()}}>
                Next
            </Button>

            <StyledDialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
                fullWidth
                maxWidth="md"
            >
                <TabPanel handleClose={handleClose} handleModalReset={handleModalReset}/>
            </StyledDialog>
        </div>
    );
}
export default Modal;