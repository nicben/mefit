import { Navbar } from 'react-bootstrap';
import {NavLink, withRouter, useParams} from "react-router-dom";
import styles from './Navbar.module.css';
import {connect, useDispatch} from "react-redux";
import { loginRefresh} from "../../store/actions/authActions";
import {getSets, overviewActionFetchExercises} from "../../store/actions/overviewActions";
import './Navbar.module.css';
import {programActionFetch} from "../../store/actions/programActions";
import {useHistory} from "react-router-dom";
import Button from "@mui/material/Button";
import React from "react";
import {
    AppBar,
    Toolbar,
    CssBaseline,
    makeStyles, Link,
} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
    navbar: {
        backgroundColor: "black",
        padding: "0 2rem",
        height: "5rem"
    },
    logo: {
        cursor: "pointer",
        marginTop: "1rem",
    },
    link: {
        textDecoration: "none",
        color: "white",
        fontSize: "20px",
        marginLeft: theme.spacing(7),
        "&:hover": {
            color: "#A1292B",
            borderBottom: "1px solid #A1292B",
        },
        "&.active": {
            color: "#A1292B",
            borderBottom: "1px solid #A1292B",
        },
    },
    userItem: {
        marginTop: "2.5rem",
        textDecoration: "none",
        color: "white",
        fontSize: "20px",
        marginRight: theme.spacing(7),
        cursor: "pointer",
        display: "flex",
    },
    userName: {
        margin: ".2rem .2rem"
    },
    logoLogin: {
        cursor: "pointer",
        marginTop: "1rem",
        float: "left",
    },
}));

const NavBar = (props) => {
    const { page } = useParams();
    const dispatch = useDispatch();
    const tokenKey = localStorage.getItem("meFitLoginToken");
    const classes = useStyles();

    const history = useHistory();

    const routeProfile = () =>{
        let path = 'profile';
        history.push(path);
    }

    const routeDashboard = () =>{
        let path = 'dashboard';
        history.push(path);
    }

    if (!props.auth && tokenKey !== null) {
        dispatch(loginRefresh(tokenKey));
        dispatch(getSets())
        dispatch(programActionFetch())
        dispatch(overviewActionFetchExercises())
    }

    return (
        <>
            <AppBar position="static" className={classes.navbar}>
                {props.auth ?
                    <Toolbar>
                        <Grid
                            justify="space-between"
                            container
                            spacing={20}
                        >
                            <Grid item>
                                <div>
                                    <img className={classes.logo} onClick={routeDashboard} src={'/Assets/mefit-logo.png'} height="100px"/>
                                    <NavLink className={classes.link} to={"/dashboard"}> Dashboard </NavLink>
                                    <NavLink className={classes.link} to={"/goals"}> Goals </NavLink>
                                    <NavLink className={classes.link} to={"/overview"}> Overview </NavLink>
                                    {props.admin === 3 &&
                                        <NavLink className={classes.link} to={"/admin"}> Admin </NavLink>
                                    }
                                </div>
                            </Grid>
                            <Grid item>
                                <NavLink style={{color: '#FFFFFF'}} to={"/profile"} className={classes.userItem}>
                                    <img src={props.image} height="40px"/>
                                    <div className={classes.userName}>{props.name}</div>
                                </NavLink>

                            </Grid>
                        </Grid>
                        <Grid>
                            <Button variant="contained"  style={{backgroundColor: '#747575', fontSize: "17px",}} onClick={()=>{localStorage.clear();window.location.reload(false)}}>Logout</Button>
                        </Grid>
                    </Toolbar>
                    :
                    <Toolbar>
                        <img className={classes.logoLogin} onClick={routeDashboard} src={'/Assets/mefit-logo.png'} height="100px"/>
                    </Toolbar>
                }
            </AppBar>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        role: state.auth.role,
        auth: state.auth.isAuthenticated,
        image: state.auth.imageUrl,
        name: state.auth.firstName,
        contributor: state.auth.contributor,
        admin: state.auth.admin,

    };
}

export default withRouter(connect(mapStateToProps)(NavBar));
