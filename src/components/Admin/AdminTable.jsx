import * as React from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Button from "@mui/material/Button";
import {useEffect, useState} from "react";
import {adminActionDeleteContributor, adminActionReplyRequest} from "../../store/actions/adminActions";
import {useDispatch} from "react-redux";

const columns = [
    { id: 'id', label: 'USERID', minWidth: 100, width: 200 },
    { id: 'firstName', label: 'FIRSTNAME', minWidth: 100, width: 200 },
    { id: 'lastName', label: 'LASTNAME', minWidth: 100, width: 200 },
];

const AdminTable = props => {
    const {list, isContributorList } = props;
    const rows = list;
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const dispatch = useDispatch();

    useEffect(() => {

    }, [rows])

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    const handleAcceptClicked = id => {
        dispatch(adminActionReplyRequest(true, id))

    };

    const handleDenyClicked = id => {
        dispatch(adminActionReplyRequest(false, id))

    };

    const handleRevokeClicked = id => {
        dispatch(adminActionDeleteContributor(id))

    };

    return (
        <Paper sx={{ width: '100%', overflow: 'hidden' }}>
            <TableContainer sx={{ maxHeight: 440 }}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                            <TableCell>
                                ACTIONS
                            </TableCell>
                            <TableCell>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map((row) => {

                                return (
                                    <TableRow
                                        hover
                                        //onClick={(event) => handleClick(event, row.name)}
                                        role="checkbox"
                                        //aria-checked={isItemSelected}
                                        tabIndex={-1}
                                        key={row.id}
                                        //selected={isItemSelected}
                                    >
                                        <TableCell key={row.id} >
                                            {row.id}
                                        </TableCell>

                                        <TableCell key={row.id} >
                                            {row.firstName}
                                        </TableCell>

                                        <TableCell key={row.id} >
                                            {row.lastName}
                                        </TableCell>

                                        { isContributorList ?
                                            <TableCell >
                                                <Button
                                                    className="action-btn"
                                                    color="error"
                                                    variant="outlined"
                                                    onClick={() => handleRevokeClicked(row.id)}
                                                >
                                                    Revoke
                                                </Button>
                                            </TableCell>
                                            :
                                            <>
                                                <TableCell >
                                                    <Button
                                                        className="action-btn"
                                                        color="primary"
                                                        variant="outlined"
                                                        onClick={() => handleAcceptClicked(row.id)}
                                                    >
                                                        Accept
                                                    </Button>
                                                </TableCell>
                                                <TableCell >
                                                    <Button
                                                        className="action-btn"
                                                        color="error"
                                                        variant="outlined"
                                                        onClick={() => handleDenyClicked(row.id)}
                                                    >
                                                        Deny
                                                    </Button>
                                                </TableCell>
                                            </>
                                        }
                                    </TableRow>
                                );
                            })}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </Paper>
    );
}

export default AdminTable

