import {Table} from "@mui/material";
import WorkoutListItem from "../WorkoutListItem/WorkoutListItem";

const WorkoutList = () => {

    return(
        <Table>
            <tbody>
            <tr className="coloredHeader">
                <th className="headerText">Title</th>
                <th className="headerText">Workout Type</th>
                <th className="headerText"></th>
                <th style={{float:"right",marginRight:"15%"}} className="headerText">Actions</th>

            </tr>
            <WorkoutListItem />
            </tbody>
        </Table>
    )
}

export default WorkoutList;