import {Box, MenuItem, FormControl, Select} from "@mui/material";
import {useSelector} from "react-redux";
import {useEffect} from "react";

const SelectedExerciseListItem = (props) => {
    const {tabIndex, exercise, setExercise} = props
    const numOfQuestions = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
    const {selectedExercises, program, selectedValue} = useSelector(state => state.userGoal)

    useEffect(() => {
        //console.log("DETTE ER METODEN!! " +checkIfAvailableSets())
        //console.log("Dette er posisjon 0 i selectedValue sin tabId: "+selectedValue[0][0].tabId)
    }, [])
    const handleChangeSet = (id, e) => {
        const exerciseObj = exercise;
        let currentTab = exerciseObj[tabIndex]

        let idExist = false;
        let index = -1
        for(const exercise of currentTab){
            index++
            if(exercise.exerciseId === id){
                idExist = true;
                break;
            }
        }
        if(idExist){
            let currentExerciseObj = currentTab[index]
            currentExerciseObj["sets"] = e.target.value
        }
        else{
            currentTab.push({tabId: tabIndex, exerciseId: id, sets: e.target.value})
        }
        setExercise(exerciseObj)

    };

    const handleChangeRep = (id, e) => {
        const exerciseObj = exercise;
        let currentTab = exerciseObj[tabIndex]

        let idExist = false;
        let index = -1
        for(const exercise of currentTab){
            index++
            if(exercise.exerciseId === id){
                idExist = true;
                break;
            }
        }
        if(idExist){
            let currentExerciseObj = currentTab[index]
            currentExerciseObj["reps"] = e.target.value
        }
        else{
            currentTab.push({tabId: tabIndex, exerciseId: id, reps: e.target.value})
        }
        setExercise(exerciseObj)
    };

    //TODO: Bare droppe denne??
    const checkIfAvailableSets = () => {
        for(let i = 0; i < 7; i++){
        }
        selectedValue.map(value => value.map(one => one.map(test => test.sets)))
        //value.state.sets !== 1 && value.state.tabId === tabIndex && value.state.exerciseId === exercise.id ? value.state.sets : number)
    }

    if(selectedExercises.length){
        return selectedExercises.map(exercise =>
            <tr key={exercise.id}>
                <td>{exercise.name}</td>
                <td>
                    <Box margin={"0.5rem"}>
                        <FormControl fullWidth>
                            <Select defaultValue={1} onChange={e => handleChangeSet(exercise.id, e)}>
                                {numOfQuestions.map((number) =>
                                    <MenuItem key={number} value={number}>{number}</MenuItem>
                                )}
                            </Select>
                        </FormControl>
                    </Box>
                </td>
                <td>
                    <Box margin={"0.5rem"}>
                        <FormControl fullWidth>
                            <Select defaultValue={1} onChange={e => handleChangeRep(exercise.id, e)}>
                                {numOfQuestions.map((number) =>
                                    <MenuItem key={number} value={number}>{number}</MenuItem>
                                )}
                            </Select>
                        </FormControl>
                    </Box>
                </td>
            </tr>
        )
    }
    else { //TODO: Trengs denne elsen lenger??
        return program.map(workout => workout.tabId === tabIndex &&
            workout.exercises.map(exercise =>
                <tr key={exercise.id}>
                    <td>{exercise.name}</td>
                    <td>
                        <Box margin={"0.5rem"}>
                            <FormControl fullWidth>
                                <Select onChange={e => handleChangeSet(exercise.id, e)}>
                                    {numOfQuestions.map((number) =>
                                        <MenuItem key={number} value={number}>{number}</MenuItem>
                                    )}
                                </Select>
                            </FormControl>
                        </Box>
                    </td>
                    <td>
                        <Box margin={"0.5rem"}>
                            <FormControl fullWidth>
                                <Select onChange={e => handleChangeRep(exercise.id, e)}>
                                    {numOfQuestions.map((number) =>
                                        <MenuItem key={number} value={number}>{number}</MenuItem>
                                    )}
                                </Select>
                            </FormControl>
                        </Box>
                    </td>
                </tr>
            )
        )
    }
}
export default SelectedExerciseListItem;