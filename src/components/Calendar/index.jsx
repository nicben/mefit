import React, {useState} from 'react';
import moment from 'moment';
import Grid from '@material-ui/core/Grid';

import CalendarBody from './CalendarBody';
import CalendarHead from './CalendarHead';
import {useSelector} from "react-redux";
import {withRouter} from "react-router-dom";


const Calendar = () => {
    console.log('Main-Index.render')

    const {goals, hasGoal, inProcess, isLoading} = useSelector(state => state.goals)

    let defaultSelectedDay = {
        day : moment().format("D"),
        month: moment().month()
    };


    const [dateObject, setdateObject] = useState(moment());
    const [showMonthTable, setShowMonthTable] = useState(false);
    const [selectedDay, setSelected] = useState(defaultSelectedDay);

    //CALENDAR HEAD
    const allMonths = moment.months();
    const currentMonth = () => dateObject.format("MMMM");
    const currentYear = () => dateObject.format("YYYY");

    const setMonth = month => {
        let monthNo = allMonths.indexOf(month);
        let newDateObject = Object.assign({}, dateObject);
        newDateObject = moment(dateObject).set("month", monthNo);
        setdateObject(newDateObject);
        setShowMonthTable(false);
    }

    const toggleMonthSelect = () => setShowMonthTable(!showMonthTable);

    // CALENDAR BODY
    const setSelectedDay = day => {
        setSelected({
            day,
            month: currentMonthNum()
        });
    };

    const currentMonthNum = () => dateObject.month();
    const daysInMonth = () => dateObject.daysInMonth();
    const currentDay = () => dateObject.format("D");
    const actualMonth = () => moment().format("MMMM");
    const actualYear = () => moment().year();

    const firstDayOfMonth = () => moment(dateObject).startOf("month").format("d");


    const getWeek = (day, month, year) => {
        let goalWeek = []
        let num = 6

        const date = `${year}-${month}-${day}`;
        const startDate = moment(date,"YYYY-MM-DD");
        goalWeek.push((startDate).format("YYYY-MM-DD").replace(/\b0+/g, ''));
        while(num !== 0){
            startDate.add(1, 'day');
            goalWeek.push(startDate.format("YYYY-MM-DD").replace(/\b0+/g, ''));
            num--
        }
        //console.log("index " + goalWeek.map(x => x));
        return goalWeek;
    }


    return (

            <Grid container spacing={3} >
                <Grid item xs={12} md={8} lg={9}>
                    <CalendarHead
                        allMonths={allMonths}
                        currentMonth={currentMonth}
                        currentYear={currentYear}
                        setMonth={setMonth}
                        showMonthTable={showMonthTable}
                        toggleMonthSelect={toggleMonthSelect}
                    />
                    <CalendarBody
                        isLoading={isLoading}
                        firstDayOfMonth={firstDayOfMonth}
                        daysInMonth={daysInMonth}
                        currentDay={currentDay}
                        currentMonth={currentMonth}
                        currentDate={defaultSelectedDay}
                        currentMonthNum={currentMonthNum}
                        actualMonth={actualMonth}
                        actualYear={actualYear}
                        setSelectedDay={setSelectedDay}
                        selectedDay={selectedDay}
                        weekdays={moment.weekdays()}
                        goals={goals}
                        getWeek={getWeek}
                        hasGoal={hasGoal}
                        inProcess={inProcess}
                    />
                </Grid>
            </Grid>
    )
};


export default withRouter(Calendar);