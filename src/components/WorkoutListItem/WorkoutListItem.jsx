import {Button} from "react-bootstrap";
import React, {useEffect, useState} from "react";
import {connect, useDispatch, useSelector} from "react-redux";
import {
    goalWorkoutActionComplete
} from "../../store/actions/goalActions";
import {withRouter} from "react-router-dom";
import {
    Box,
    Modal,
    Table,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    TableContainer,
    Paper
} from "@mui/material";

const WorkoutListItem = (props) => {
    const {goalWorkouts, progress} = useSelector(state => state.goals)
    const [listOfWorkouts, setListOfWorkouts] = useState([]);
    const dispatch = useDispatch();
    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => {
        setOpen(false)
    }

    const [displayWorkout, setDisplayWorkout] = useState(false);

    const [displayWorkoutId, setDisplayWorkoutId] = useState(0);

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };
    const workoutClicked = (id) =>{
        setDisplayWorkout(true)
        setDisplayWorkoutId(id)
        handleOpen()
    }

    const workouts = () => {
        return (
            <>
                <TableContainer style={{maxHeight: 500}} component={Paper}>
                    <Table stickyHeader sx={{minWidth: 300}} wrapperStyle={{maxHeight: 100}}>
                        <TableHead style={{zIndex: 1}} align={"center"}>
                            <TableRow style={{cursor: 'pointer'}} align={"center"}>
                                <TableCell>Exercise</TableCell>
                                <TableCell>Reps</TableCell>
                                <TableCell>Sets</TableCell>
                            </TableRow>
                        </TableHead>
                        {sets()}
                    </Table>
                </TableContainer>
            </>
        )
    }
    useEffect(() => {
        setListOfWorkouts([...goalWorkouts])
    }, [progress])

    const finishWorkoutBtnClicked = (id) => {
        listOfWorkouts.map(workout => {
            if (workout.id === id) {
                console.log("WORKOUT LIST ITEM - complete workout")
                dispatch(goalWorkoutActionComplete(workout))
                return workout.completed = true
            }
        })
    }

    const sets = () => {
        console.log("display wokroutid " + displayWorkoutId)
        let setId = []
        for (const workout of props.workouts) {
            if(workout.id===displayWorkoutId){
                for(const id of workout.sets){
                    setId.push(id);
                }
            }
        }

        let row = []
        for(const id of setId){
            for (const set of props.sets){
                if(id === set.id){
                    row.push({exercise: set.exercise.name, sets: set.sets, reps: set.reps,id:id})
                }
            }
        }
        let newRow = [];
        for (let i = 0; i < row.length; i++) {
            newRow.push(row[i]);
        }

        return(
            <TableBody >
                {newRow.map((field) => (
                    <TableRow key={field.id}>
                        <TableCell component="th" scope="row">
                            {field.exercise}
                        </TableCell>
                        <TableCell>{field.sets}</TableCell>
                        <TableCell>{field.reps}</TableCell>
                    </TableRow>
                ))}

            </TableBody>
        )
    }

    return listOfWorkouts.map((workout, index) =>
        workout &&
        <tr key={index} className="coloredRows">
            <td className="rowText">{workout.title}</td>
            <td className="rowText">{workout.type}</td>
            {/*<td>{workout.exercises.map(exercise =>*/}
            {/*    `${exercise}, `*/}
            {/*)}*/}
            {/*</td>*/}
            <td className="rowText"></td>
            <td className="rowText">
                {workout.completed === 0 ?
                    <>
                        <Button
                            style={{
                                backgroundColor: "var(--button-color)",
                                paddingLeft: "3%",
                                marginRight: "2rem",
                                paddingRight: "6%",
                                float: "right"
                            }}
                            onClick={() =>workoutClicked(workout.id)}
                        >
                            View details
                        </Button>
                        <Button
                            id={workout.id}
                            className="complete-btn"
                            style={{
                                backgroundColor: "var(--button-color)",
                                paddingLeft: "3%",
                                marginRight: "2rem",
                                paddingRight: "6%",
                                float: "right"
                            }}
                            onClick={() => finishWorkoutBtnClicked(workout.id)}
                        >
                            Finish
                        </Button>

                        <Modal
                            open={open}
                            onClose={handleClose}
                            aria-labelledby="modal-modal-title"
                            aria-describedby="modal-modal-description"
                        >
                            <Box sx={style}>
                                {workouts()}
                            </Box>
                        </Modal>

                    </>
                    :
                    <p style={{
                        marginRight: "2rem",
                        float: "right"
                    }}
                    >Completed</p>
                }
            </td>

        </tr>
    );
}


const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        exercises: state.exercises,
        workouts: state.workouts.workouts,
        sets : state.overview.sets
    };
}


export default withRouter(connect(mapStateToProps)(WorkoutListItem));
