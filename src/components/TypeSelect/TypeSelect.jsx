import * as React from 'react';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import MenuItem from "@mui/material/MenuItem";
import {useSelector} from "react-redux";
import {useEffect} from "react";

const TypeSelect = (props) => {
    const {type, setStateOfParent, tabIndex, tabWorkoutType, setStateOfWorkoutType} = props;
    const {workoutType} = useSelector(state => state.userGoal);
    const types = ["Do not want to specify","Strength","Balance","Flexibility and Mobility","Aerobics","Coordination and Agility","Other"]

    useEffect(() => {
        initialStateWorkoutType();
    })

    const initialStateWorkoutType = () => {
        return JSON.stringify(workoutType)
    }

    const newStateWorkoutType = () => {
        return JSON.stringify(tabWorkoutType[tabIndex]);
    }

    console.log("DETTE ER INITIALSTATEMETODE:  "+ initialStateWorkoutType())
    console.log("DETTE ER NEWSTATEMETODE:  "+ newStateWorkoutType())

    const handleChange = (event) => {
        let workoutTypeObj = JSON.parse(JSON.stringify(tabWorkoutType));
        workoutTypeObj[tabIndex] = event.target.value;
        setStateOfParent(event.target.value);
        setStateOfWorkoutType(workoutTypeObj);
    };

    return (
        <Box sx={{ minWidth: 120 }}>
            <FormControl fullWidth>
                <h4>Choose a workout type:</h4>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={type}
                    label="Type of workout"
                    onChange={handleChange}
                >
                    {types.map((type,index) =>
                        <MenuItem key={index} value={type}>{type}</MenuItem>
                    )}
                </Select>
            </FormControl>
        </Box>
    );
}
export default TypeSelect;