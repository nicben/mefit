import ProgressBar from 'react-bootstrap/ProgressBar';
import styles from './Progress.module.css'
import {useSelector} from "react-redux";

const Progress = () => {
    console.log('Progress.render')

    const {progress} = useSelector(state => state.goals)

    return (
        <>
            <ProgressBar className={styles.progressbar} now={progress} label={`${progress}%`}/>
        </>
    )
}

export default Progress