import Login from "../components/Auth/Login";
import './LoginPage.css';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {CircularProgress} from "@material-ui/core";
import Box from "@mui/material/Box";
import LoadingComponent from "../components/UserFeedback/LoadingComponent";

const LoginPage = (props) => {

    console.log('LoginPage.render')

    // const handleClicked = () => {
    // }
    console.log(props)
    const isLoading = () => {
        if(props.auth.loading){
            return(
                <Box
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    minHeight="100vh"
                >
                    <CircularProgress />
                </Box>

               )
        }
        else return "";
    }
    return (
        <>
            {isLoading()}
            <Login/>
        </>

    );
};
const mapStateToProps = (state) => {
    console.log(state.exercises)
    return {
        auth: state.auth,

    };
}
export default withRouter(connect(mapStateToProps)(LoginPage))
