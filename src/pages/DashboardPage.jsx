import Button from '@mui/material/Button';
import Progress from "../components/Progress/Progress";
import AppContainer from "../hoc/AppContainer";
import Calendar from "../components/Calendar";
import '../App.css';
import {
    goalActionDelete,
    goalActionInProcess,
    goalActionModalOpen,
    goalActionNew,
    goalActionSelected
} from "../store/actions/goalActions";
import {useDispatch, useSelector} from "react-redux";
import LoadingComponent from "../components/UserFeedback/LoadingComponent";
import Modal from "../components/Modal/Modal";
import React from "react";
import moment from "moment";
import {Box} from "@material-ui/core";
import Paper from "@mui/material/Paper";

const DashboardPage = () => {
    console.log('Main.render')

    const {goalWorkouts, goalId, hasGoal, modalOpen, inProcess, currentGoal} = useSelector(state => state.goals)
    const dispatch = useDispatch();

    const handleDeleteClicked = () => {
        dispatch(goalActionDelete(goalId))
    }

    const handleAddClicked = () => {
        dispatch(goalActionInProcess(true))
        dispatch(goalActionSelected(moment(new Date()).format("YYYY-MM-DD")))
    }

    const handleNextClicked = () => {
        dispatch(goalActionModalOpen(true))
        dispatch(goalActionNew())
    }

    const handleCancelClicked = () => {
        dispatch(goalActionDelete(goalId))
        dispatch(goalActionInProcess(false))
    }

    return (
        <AppContainer>
            <div className="pageContainer">
                <LoadingComponent/>
                <h1>Dashboard</h1>
                {hasGoal &&
                    <>
                        <p>Progress:</p>
                        <Progress/>
                    </>
                }
                <Box sx={{ display: 'flex'}}
                     style={{
                         width: '100%',
                         position: 'relative',
                     }}>
                    <div
                        style={{
                            top: 0,
                            width: '100%',
                        }}
                    >
                        <Calendar />
                        <Box sx={{ pt: 2 }} style={{ display: "inline-flex"}}>
                            {hasGoal && !modalOpen ?
                                <Button sx={{ mr: 2 }} variant="outlined" color="error"  onClick={handleDeleteClicked}>Delete goal</Button>
                                :
                                <>
                                    <Button sx={{ mr: 2 }} disabled={inProcess} className="add-btn" variant="outlined" onClick={handleAddClicked}>Create new goal</Button>
                                    <Modal disabled={!inProcess} className="next-btn" handleAddClicked={handleNextClicked}/>
                                    <Button sx={{ mx: 2 }} disabled={!inProcess} variant="outlined" color="error" onClick={handleCancelClicked}>Cancel</Button>
                                </>
                            }
                        </Box>
                    </div>
                    {hasGoal && !modalOpen &&
                    <Paper
                        sx={{width: '16rem', height: '25.3rem', overflow: 'hidden', p: 1}}
                        elevation={1}
                        style={{
                            padding: 20,
                            position: 'absolute',
                            right:0
                        }}>
                        <h4>{currentGoal.title}</h4>
                        <br/>
                        <h6>Workouts</h6>

                        {goalWorkouts.map((workout, index) => (
                            <ul key={index}>
                                <li>{workout.type} </li>
                            </ul>
                        ))}
                        <br/>
                    </Paper>
                    }
                </Box>
            </div>
        </AppContainer>
    );
};

export default DashboardPage;