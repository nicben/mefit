import './App.css';
import {
    BrowserRouter,
    Switch,
    Route,
} from "react-router-dom";
import NavBar from "./components/Navbar/NavBar";
import LoginPage from "./pages/LoginPage";
import DashboardPage from "./pages/DashboardPage";

import ProfilePage from "./pages/ProfilePage";
import AdminPage from "./pages/AdminPage";
import GoalPage from "./pages/GoalPage";
import ViewPage from "./pages/ViewPage";
import NotFoundPage from "./pages/NotFoundPage";

function App() {
    console.log('App.render')


  return (
      <BrowserRouter>
          <Route path='/:page' component={NavBar} />
          <Route exact path='/' component={NavBar} />
          <div className="App">
              <Switch>
                  <Route path="/" exact component={LoginPage}/>
                  <Route path="/dashboard" component={DashboardPage}/>
                  <Route path="/profile" component={ProfilePage}/>
                  <Route path="/admin" component={AdminPage}/>
                  <Route path="/goals" component={GoalPage}/>
                  <Route path="/overview" component={ViewPage}/>
                  <Route path="*" component={NotFoundPage}/>
              </Switch>
          </div>
      </BrowserRouter>
  );
}


export default App;



