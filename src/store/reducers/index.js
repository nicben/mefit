import {combineReducers} from "redux";
import {authReducer} from "./authReducers";
import {goalReducer} from "./goalReducer";
import {workoutReducer} from "./workoutReducer";
import {programReducer} from "./programReducer";
import {exerciseReducer} from "./exerciseReducer";
import {adminReducer} from "./adminReducer";
import {userGoalReducer} from "./userGoalReducer";
import {accountReducer} from "./accountReducers";
import {overviewReducer} from "./overviewReducers";

const appReducer = combineReducers({
    admin: adminReducer,
    goals: goalReducer,
    programs: programReducer,
    workouts: workoutReducer,
    exercises: exerciseReducer,
    auth: authReducer,
    userGoal: userGoalReducer,
    account: accountReducer,
    overview : overviewReducer
})

export default appReducer
