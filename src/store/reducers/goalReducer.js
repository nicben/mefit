import * as types from '../constants/ActionTypes'

const initialState = {
    currentGoal: null,
    goals: [],
    goalWorkouts: [],
    goalExercises: [],
    completedGoals: [],
    completedWorkouts: [],
    newGoalDate: null,
    inProcess: false,
    modalOpen: false,
    hasGoal: false,
    goalCompleted: false,
    goalId: null,
    progress: 0,
    goalError: ''
}

export const goalReducer = (state = initialState , action) => {

    switch (action.type) {

        case types.ACTION_GOAL_FETCH:
            return {
                ...state
            }

        case types.ACTION_GOAL_SET:
            const currentGoal = action.payload.filter(goal => goal.completed === false)
            return {
                ...state,
                currentGoal: currentGoal[0],
                completedGoals: action.payload.filter(goal => goal.completed === true),
                goals: action.payload
            }

        case types.ACTION_GOAL_SELECTED:
            return {
                ...state,
                newGoalDate:{ startDate: action.payload } ,
                hasGoal: false,
                modalOpen: false,
                progress: 0,
            }

        case types.ACTION_GOAL_IN_PROCESS:
            return {
                ...state,
                inProcess: action.payload
            }

        case types.ACTION_GOAL_NEW:
            return {
                ...state
            }

        case types.ACTION_GOAL_ADD:
            return {
                ...state,
                currentGoal: action.payload,
                hasGoal: true,
                goals: [...state.goals, action.payload],
                goalId: action.payload.id,
                progress: 0,
            }

        case types.ACTION_GOAL_COMPLETED:
            const CompletedList = state.goals.filter(goal => goal.id !== action.payload.id)
            return {
                ...state,
                completedGoals: [...state.completedGoals, state.currentGoal],
                goals: CompletedList,
                progress: 0
            }

        case types.ACTION_GOAL_CHANGE_LIST:
            return {
                ...state,
                goals: [...state.goals, action.payload]
            }

        case types.ACTION_GOAL_COMPLETED_STATUS:
            return {
                ...state,
                goalCompleted: true
            }


        case types.ACTION_GOAL_CALC_PROGRESS:
            return {
                ...state
            }

        case types.ACTION_GOAL_SET_PROGRESS:
            return {
                ...state,
                progress: Math.floor(action.payload)
            }


        case types.ACTION_GOAL_DELETE:
            const newList = state.goals.filter(goal =>
                goal.id !== state.goalId);
            return {
                ...state,
                goals: newList,
                progress: 0,
            }

        case types.ACTION_GOAL_CLEAR:
            return {
                ...state,
                currentGoal: null,
                goalWorkouts: [],
                goalExercises: [],
                completedWorkouts: [],
                newGoalDate: null,
                hasGoal: false,
                goalCompleted: false,
                goalId: null,
                progress: 0,
                modalOpen: false,
                inProcess: false
            }

        case types.ACTION_GOAL_TRUE:
            return {
                ...state,
                hasGoal: true,
                goalId: action.payload
            }

        case types.ACTION_GOAL_MODAL_OPEN:
            return {
                ...state,
                modalOpen: action.payload
            }

        case types.ACTION_GOAL_ERROR:
            return {
                ...state,
                goalError: action.payload
            }




        case types.ACTION_GOAL_WORKOUT_FETCH:

            return {
                ...state
            }

        case types.ACTION_GOAL_WORKOUT_SET:

            return {
                ...state,
                goalWorkouts: action.payload
            }


        case types.ACTION_GOAL_WORKOUT_COMPLETE:
            const notCompletedList = state.goalWorkouts.filter(workout => workout.id !== action.payload.id)
            return {
                ...state,
                goalWorkouts: notCompletedList
            }

        case types.ACTION_GOAL_WORKOUT_CHANGE_LIST:
            return {
                ...state,
                goalWorkouts: [...state.goalWorkouts, action.payload]
            }

        case types.ACTION_GOAL_WORKOUT_COMPLETED_LIST:
            return {
                ...state,
                completedWorkouts: [...state.completedWorkouts, action.payload]
            }

        case types.ACTION_GOAL_WORKOUT_ERROR:
            return {
                ...state,
            }





        case types.ACTION_GOAL_EXERCISE_FETCH:
            return {
                ...state,
            }

        case types.ACTION_GOAL_EXERCISE_SET:
            return {
                ...state,
                goalExercises: [...state.goalExercises,action.payload]
            }

        default:
            return state
    }
}
