import * as types from '../constants/ActionTypes'

const initialState = {
    sets: [],
    requests: [],
    contributors: [],
    overviewError: ''
}

export const overviewReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ACTION_OVERVIEW_GET_SETS:
            return {
                ...state
            }
        case types.ACTION_OVERVIEW_SET_SETS:
            return {
                sets: action.payload
            }

        case types.ACTION_OVERVIEW_FETCH_PROGRAMS:
            return {
                ...state
            }
        case types.ACTION_OVERVIEW_SET_PROGRAMS:
            return {
                ...state,
                requests: action.payload
            }
        case types.ACTION_OVERVIEW_FETCH_WORKOUTS:
            return {
                ...state
            }
        case types.ACTION_OVERVIEW_SET_WORKOUTS:
            return {
                ...state,
                requests: action.payload
            }
        case types.ACTION_OVERVIEW_FETCH_EXERCISES:
            return {
                ...state
            }
        case types.ACTION_OVERVIEW_SET_EXERCISES:
            return {
                ...state,
                requests: action.payload
            }
        case types.ACTION_OVERVIEW_ERROR:
            return {
                ...state,
                overviewError: action.payload
            }

        case types.ACTION_OVERVIEW_ADD_EXERCISES:
            return{
                ...state
            }
        case types.ACTION_OVERVIEW_CREATE_SETS:
            return {
                ...state
            }
        default:
            return state
    }
}