import * as types from '../constants/ActionTypes'


const initialState = {
    exercises: [],
    isLoading: true,
    error: ''
}

export const exerciseReducer = (state = initialState , action) => {

    switch (action.type) {
        case types.ACTION_OVERVIEW_SET_EXERCISES:
            return {
                exercises: action.payload,
                isLoading: false,
                loginError: ''
            }

        case types.ACTION_EXERCISE_FETCH:
            return {
                ...state,
                isLoading: true,
                loginError: ''
            }

        case types.ACTION_EXERCISE_SET:
            return {
                exercises: action.payload,
                isLoading: true,
                loginError: ''
            }

        case types.ACTION_EXERCISE_ADD:
            return {
                ...state,
                exercises: [...action.payload, ...state],
                loginError: ''
            }

        case types.ACTION_EXERCISE_DELETE:
            return {
                ...state,
                exercises: [...action.payload],
            }

        case types.ACTION_EXERCISE_ERROR:
            return {
                ...state,
                exerciseError: action.payload
            }

        default:
            return state
    }
}

export const getAllWorkouts = (state) => state.exercise.exercises