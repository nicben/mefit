import * as types from '../constants/ActionTypes'

const initialState = {
    program: [],
    //programTitle: "",
    selectedExercises: [],
    tabId: 0,
    workoutType: "Do not want to specify",
    error: ""
}

export const userGoalReducer = (state = initialState , action) => {

    switch (action.type) {
        case types.ACTION_USER_CREATE_SET:
            return {
                ...state,
                selectedExercises: [ ...state.selectedExercises, action.payload ],
            }
        // case types.ACTION_USER_SET_SET:
        //     return{
        //         ...state,
        //         selectedValue: [...state.selectedValue, {tabId: action.payload.tabId, workouts: [...action.payload]}]
        //     }
        case types.ACTION_USER_EXERCISE_ADD_SELECTED:
            return {
                ...state,
                selectedExercises: [ ...state.selectedExercises, action.payload ]
            }
        case types.ACTION_USER_ADD_WORKOUT:
            return {
                ...state,
                program: [...state.program, {tabId: action.tabId, type: action.workoutType, exercises: [...action.workout]}],
                selectedExercises: [],
                workoutType: initialState.workoutType
            }
        case types.ACTION_USER_ADD_MORE_EXERCISES_TO_WORKOUT:
            let index = 0;
            for(let i = 0; i < state.program.length; i++){
                if(action.tabId === state.program[i].tabId){
                    index=i;
                }
            }
            const newArray = state.program.filter(item => item.tabId !== action.tabId)

            return {
                ...state,
                selectedExercises: state.program[index].exercises ,
                program: newArray,
                workoutType: action.workoutType
            }
        case types.ACTION_USER_ADDED_GOAL:
            return {
                ...state,
                program: [],
                selectedExercises: [],
                workoutType: initialState.workoutType
            }
        case types.ACTION_USER_RESET_MODAL_INPUT:
            return {
                ...state,
                program: [],
                selectedExercises: [],
                workoutType: initialState.workoutType
            }
        case types.ACTION_USER_CREATE_WORKOUT_ERROR:
            return {
                ...state,
                error: action.payload
            }
        case types.ACTION_USER_CREATE_GOAL_ERROR:
            return {
                ...state,
                error: action.payload
            }
        default:
            return state
    }
}
