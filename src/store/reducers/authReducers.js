import * as types from '../constants/ActionTypes'

const initialState = {
    error: "",
    loading: false,
    userId: 0,
    userToken: '',
    isAuthenticated: false,
    firstName:'',
    lastName:'',

    imageUrl:'',
    email:'',
    fitnessLevel: 0,
    googleID: '',
    height: 0,
    weight: 0,
    admin: 0,
    contributor: 0
}

export const authReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.LOGIN:
            return {
                ...state,
                loading: true
            }

        case types.SET_USER:
            return {
                ...state,
                isAuthenticated: true,
                firstName: action.payload.account.firstName,
                lastName: action.payload.account.lastName,
                userId: action.payload.account.id,
                email:action.payload.account.email,
                fitnessLevel: action.payload.account.fitnessLevel,
                googleID: action.payload.account.googleId,
                height: action.payload.account.height,
                weight: action.payload.account.weight,
                admin: action.payload.account.admin,
                contributor: action.payload.account.contributer,
                userToken: action.payload.token,
                imageUrl: action.payload.account.imageUrl,
                loading: false
            }


        case types.LOGOUT:
            return {
                ...state,
                userToken: '',
                isAuthenticated: false,
                firstName:'',
                lastName:'',

            }
        case types.LOGIN_REFRESH:
            return {
                ...state,
                loading: true

            }
        case types.ACTION_ACCOUNT_ERROR:
            return {
                ...state,
                error: action.payload,
                loading: false
            }
        default:
            return state
    }
};
