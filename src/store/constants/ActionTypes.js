export const ACTION_GOAL_FETCH = '[goal] FETCH'
export const ACTION_GOAL_SET = '[goal] SET'
export const ACTION_GOAL_SELECTED = '[goal] SELECTED'
export const ACTION_GOAL_IN_PROCESS = '[goal] IN_PROCESS'
export const ACTION_GOAL_NEW = '[goal] NEW'
export const ACTION_GOAL_COMPLETED = '[goal] COMPLETED'
export const ACTION_GOAL_CHANGE_LIST = '[goal] CHANGE_LIST'
export const ACTION_GOAL_COMPLETED_STATUS = '[goal] COMPLETED_STATUS'
export const ACTION_GOAL_CALC_PROGRESS = '[goal] CALC_PROGRESS'
export const ACTION_GOAL_SET_PROGRESS = '[goal] SET_PROGRESS'
export const ACTION_GOAL_ADD = '[goal] ADD'
export const ACTION_GOAL_DELETE = '[goal] DELETE'
export const ACTION_GOAL_CLEAR = '[goal] CLEAR'
export const ACTION_GOAL_TRUE = '[goal] STATUS'
export const ACTION_GOAL_MODAL_OPEN = '[goal] MODAL_STATUS'
export const ACTION_GOAL_ERROR = '[goal] ERROR'

export const ACTION_GOAL_WORKOUT_FETCH = '[goal - workout] FETCH'
export const ACTION_GOAL_WORKOUT_SET = '[goal - workout] SET'
export const ACTION_GOAL_WORKOUT_COMPLETE = '[goal - workout] COMPLETE'
export const ACTION_GOAL_WORKOUT_CHANGE_LIST = '[goal - workout] CHANGE_STATE'
export const ACTION_GOAL_WORKOUT_COMPLETED_LIST = '[goal - workout] COMPLETED_LIST'
export const ACTION_GOAL_WORKOUT_FETCH_NEW = '[goal - workout] FETCH_NEW'
export const ACTION_GOAL_WORKOUT_ERROR = '[goal - workout] ERROR'

export const ACTION_GOAL_EXERCISE_FETCH = '[goal - exercise] FETCH'
export const ACTION_GOAL_EXERCISE_SET = '[goal - exercise] SET'

export const ACTION_PROGRAM_FETCH = '[program] FETCH'
export const ACTION_PROGRAM_SET = '[program] SET'
export const ACTION_PROGRAM_NEW = '[program] NEW'
export const ACTION_PROGRAM_ADD = '[program] ADD'
export const ACTION_PROGRAM_DELETE = '[program] DELETE'
export const ACTION_PROGRAM_ERROR = '[program] ERROR'
export const ACTION_PROGRAM_CREATE = '[program] Create'
export const ACTION_PROGRAM_UPDATE = '[program] UPDATE'

export const ACTION_WORKOUT_FETCH = '[workout] FETCH'
export const ACTION_WORKOUT_SET = '[workout] SET'
export const ACTION_WORKOUT_ADD = '[workout] ADD'
export const ACTION_WORKOUT_DELETE = '[workout] DELETE'
export const ACTION_WORKOUT_ERROR = '[workout] ERROR'
export const ACTION_WORKOUT_UPDATE = '[workout] UPDATE'
export const ACTION_WORKOUT_CREATE_SET_WORKOUT = '[workout] CREATE_SET_WORKOUT'


export const ACTION_EXERCISE_FETCH = '[exercise] FETCH'
export const ACTION_EXERCISE_SET = '[exercise] SET'
export const ACTION_EXERCISE_ADD = '[exercise] ADD'
export const ACTION_EXERCISE_DELETE = '[exercise] DELETE'
export const ACTION_EXERCISE_ERROR = '[exercise] ERROR'

export const LOGIN = '[login] LOGIN'
export const SET_USER = '[login] SET_USER'
export const LOGOUT = '[login] LOGOUT'
export const LOGIN_REFRESH = '[LOGIN] LOGIN_REFRESH'

export const ACTION_CREATE_ACCOUNT = '[account] CREATE_ACCOUNT';
export const ACTION_SET_ACCOUNT = '[account] SET_ACCOUNT';
export const ACTION_GET_ACCOUNT = '[account] GET_ACCOUNT';
export const ACTION_EDIT_ACCOUNT = '[account] EDIT_ACCOUNT';
export const ACTION_SEND_REQUEST = '[account] SEND_REQUEST';
export const ACTION_ACCOUNT_ERROR = '[account] ACCOUNT_ERROR'

export const ACTION_USER_CREATE_SET = '[userGoal] CREATE'
//export const ACTION_USER_SET_SET = '[userGoal] SET SET'
export const ACTION_USER_CREATE_SET_ERROR = '[userGoal] ERROR'
export const ACTION_USER_EXERCISE_ADD_SELECTED = '[userGoal] ADD_SELECTED';
export const ACTION_USER_ADD_WORKOUT = '[userGoal] ADD_WORKOUT';
export const ACTION_USER_CHANGE_WORKOUT = '[userGoal] CHANGE_WORKOUT';
export const ACTION_USER_ADD_MORE_EXERCISES_TO_WORKOUT = '[userGoal] ADD_EXERCISE_TO_EXISTING_WORKOUT';
export const ACTION_USER_CREATE_WORKOUT = '[userGoal] CREATE_WORKOUT';
export const ACTION_USER_CREATE_WORKOUT_ERROR = '[userGoal] USER_CREATE_WORKOUT_ERROR';
export const ACTION_USER_CREATE_GOAL = '[userGoal] CREATE_GOAL';
export const ACTION_USER_ADDED_GOAL = '[userGoal] ADDED_GOAL';
export const ACTION_USER_ADD_GOAL_TITLE = '[userGoal] ADD_GOAL_TITLE';
export const ACTION_USER_RESET_MODAL_INPUT = '[userGoal] RESET_MODAL_INPUT';
export const ACTION_USER_CREATE_GOAL_ERROR = '[userGoal] USER_CREATE_GOAL_ERROR';

export const ACTION_ADMIN_FETCH_CONTRIBUTORS = '[admin] FETCH_CONTRIBUTORS';
export const ACTION_ADMIN_SET_CONTRIBUTORS = '[admin] SET_CONTRIBUTORS';
export const ACTION_ADMIN_FETCH_REQUESTS = '[admin] FETCH_REQUESTS';
export const ACTION_ADMIN_SET_REQUESTS = '[admin] SET_REQUESTS';
export const ACTION_ADMIN_REPLY_REQUEST = '[admin] REPLY_REQUEST';
export const ACTION_ADMIN_DELETE_CONTRIBUTOR = '[admin] DELETE_CONTRIBUTOR';
export const ACTION_ADMIN_ERROR = '[admin] ERROR'

export const ACTION_OVERVIEW_FETCH_PROGRAMS = '[overview] FETCH_PROGRAMS';
export const ACTION_OVERVIEW_SET_PROGRAMS = '[overview] SET_PROGRAMS';
export const ACTION_OVERVIEW_FETCH_WORKOUTS = '[overview] FETCH_WORKOUTS';
export const ACTION_OVERVIEW_SET_WORKOUTS = '[overview] SET_WORKOUTS';
export const ACTION_OVERVIEW_FETCH_EXERCISES = '[overview] FETCH_EXERCISES';
export const ACTION_OVERVIEW_SET_EXERCISES = '[overview] SET_EXERCISES';
export const ACTION_OVERVIEW_ADD_EXERCISES = '[overview] ADD_EXERCISES';
export const ACTION_OVERVIEW_GET_SETS = '[overview] GET_SETS';
export const ACTION_OVERVIEW_ERROR = '[overview] ERROR'
export const ACTION_OVERVIEW_SET_SETS = '[overview] SET_SETS'
export const ACTION_OVERVIEW_CREATE_SETS = '[overview] CREATE_SETS'
export const ACTION_OVERVIEW_CREATE_WORKOUT = '[overview] CREATE_WORKOUT'

export const CREATE_GOALS_FROM_PROGRAM = '[programGoals] CREATE_WORKOUTS'
export const CREATE_GOALS_GOAL_PROGRAMGOALS= '[programGoals] CREATE_GOAL'
export const FILL_GOAL_PROGRAMGOALS= '[programGoals] FILL_GOAL'
