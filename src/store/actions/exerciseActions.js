import * as types from '../constants/ActionTypes'


export const exerciseActionFetch = () => ({
    type: types.ACTION_EXERCISE_FETCH,
})

export const exerciseActionSet = exercises => ({
    type: types.ACTION_EXERCISE_SET,
    payload: exercises
})

export const exerciseActionAdd = exercise => ({
    type: types.ACTION_EXERCISE_ADD,
    payload: exercise
})

export const exerciseActionDelete = id => ({
    type: types.ACTION_EXERCISE_DELETE,
    payload: id
})

export const exerciseActionError = error => ({
    type: types.ACTION_EXERCISE_ERROR,
    payload: error
})