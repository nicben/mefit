import * as types from '../constants/ActionTypes'


export const adminActionFetchContributors = () => ({
    type: types.ACTION_ADMIN_FETCH_CONTRIBUTORS,
})

export const adminActionSetContributor = contributors => ({
    type: types.ACTION_ADMIN_SET_CONTRIBUTORS,
    payload: contributors
})

export const adminActionFetchRequests = () => ({
    type: types.ACTION_ADMIN_FETCH_REQUESTS,
})

export const adminActionSetRequest = requests => ({
    type: types.ACTION_ADMIN_SET_REQUESTS,
    payload: requests
})

export const adminActionReplyRequest = (status, id) => ({
    type: types.ACTION_ADMIN_REPLY_REQUEST,
    status: status,
    id: id
})

export const adminActionDeleteContributor = id => ({
    type: types.ACTION_ADMIN_DELETE_CONTRIBUTOR,
    id: id
})


export const adminActionError = error => ({
    type: types.ACTION_ADMIN_ERROR,
    payload: error
})