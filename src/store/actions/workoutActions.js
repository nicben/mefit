import * as types from '../constants/ActionTypes'
import {ACTION_WORKOUT_CREATE_SET_WORKOUT, ACTION_WORKOUT_UPDATE} from "../constants/ActionTypes";


export const workoutActionFetch = () => ({
    type: types.ACTION_WORKOUT_FETCH,
})

export const workoutActionSet = workouts => ({
    type: types.ACTION_WORKOUT_SET,
    payload: workouts
})

export const workoutActionAdd = workout => ({
    type: types.ACTION_WORKOUT_ADD,
    payload: workout
})

export const workoutActionDelete = id => ({
    type: types.ACTION_WORKOUT_DELETE,
    payload: id
})

export const workoutActionError = error => ({
    type: types.ACTION_WORKOUT_ERROR,
    payload: error
})

export const createNewSetsInWorkout = payload => ({
    type: types.ACTION_WORKOUT_CREATE_SET_WORKOUT,
    payload
})

export const updateWorkout = payload => ({
    type: types.ACTION_WORKOUT_UPDATE,
    payload
})