import * as types from '../constants/ActionTypes'
import {ACTION_PROGRAM_CREATE, ACTION_PROGRAM_UPDATE} from "../constants/ActionTypes";




export const programActionFetch = () => ({
    type: types.ACTION_PROGRAM_FETCH,
})

export const programActionSet = programs => ({
    type: types.ACTION_PROGRAM_SET,
    payload: programs
})

export const programActionNew = program => ({
    type: types.ACTION_PROGRAM_NEW,
    payload: program
})

export const programActionAdd = () => ({
    type: types.ACTION_PROGRAM_ADD
})

export const programActionDelete = id => ({
    type: types.ACTION_PROGRAM_DELETE,
    payload: id
})

export const programActionError = error => ({
    type: types.ACTION_PROGRAM_ERROR,
    payload: error
})

export const createNewProgram  = payload => ({
    type: types.ACTION_PROGRAM_CREATE,
    payload
})
export const updateProgram = payload => ({
    type: types.ACTION_PROGRAM_UPDATE,
    payload

})