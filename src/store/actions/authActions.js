import * as types from '../constants/ActionTypes'

export const login = (payload) => ({
    type: types.LOGIN,
    payload:payload
})

export const setUser = (payload) => ({
    type: types.SET_USER,
    payload:payload
})

export const logout = () => ({
    type: types.LOGOUT
})

export const loginRefresh = (payload) => ({
    type: types.LOGIN_REFRESH,
    payload: payload
})