import * as types from '../constants/ActionTypes'
import {ACTION_USER_ADD_GOAL_TITLE, ACTION_USER_ADD_TITLE_GOAL} from "../constants/ActionTypes";

export const userExerciseActionSelected = (exercise) => ({
    type: types.ACTION_USER_EXERCISE_ADD_SELECTED,
    payload: exercise
})

export const userWorkoutActionAdd = (workout, tabId, workoutType) => ({
    type: types.ACTION_USER_ADD_WORKOUT,
    workout: workout,
    tabId: tabId,
    workoutType: workoutType
})
/*
export const userSetActionSet = (exercise) => ({
    type: types.ACTION_USER_SET_SET,
    payload: exercise
})*/

export const userWorkoutActionAddExercise = (exercise, tabId) => ({
    type: types.ACTION_USER_ADD_MORE_EXERCISES_TO_WORKOUT,
    payload: exercise,
    tabId: tabId
})

export const userWorkoutActionNew = (workout, listOfTypes) => ({
    type: types.ACTION_USER_CREATE_WORKOUT,
    payload: workout,
    listOfTypes: listOfTypes
})

export const userGoalActionNew = (workouts) => ({
    type: types.ACTION_USER_CREATE_GOAL,
    payload: workouts
})

export const userGoalActionAdded = () => ({
    type: types.ACTION_USER_ADDED_GOAL
})

export const userCreateWorkoutActionError = (error) => ({
    type: types.ACTION_USER_CREATE_WORKOUT_ERROR,
    payload: error
})

export const userCreateGoalActionError = (error) => ({
    type: types.ACTION_USER_CREATE_GOAL_ERROR,
    payload: error
})

export const userSetActionCreate = (exercises, listOfTypes) => ({
    type: types.ACTION_USER_CREATE_SET,
    payload: exercises,
    listOfTypes: listOfTypes
})

export const userWorkoutActionPut = (workout) => ({
    type: types.ACTION_USER_CHANGE_WORKOUT,
    payload: workout
})

export const userGoalActionResetModalInput = () => ({
    type: types.ACTION_USER_RESET_MODAL_INPUT
})

export const userGoalActionAddProgramTitle = (title) => ({
    type: types.ACTION_USER_ADD_GOAL_TITLE,
    title: title
})

export const userSetActionError = error => ({
    type: types.ACTION_USER_CREATE_SET_ERROR,
    payload: error
})