import * as types from '../constants/ActionTypes'
import {
    goalActionAdd,
    goalActionError,
    goalActionSet,
    goalWorkoutActionSet,
    goalWorkoutActionFetch,
    goalActionClear,
    goalExerciseActionSet,
    goalWorkoutActionCompletedList,
    goalWorkoutActionError,
    goalWorkoutActionChangeList,
    goalActionTrue,
    goalActionCalcProgress,
    goalActionSetProgress,
    goalActionChangeList,
    goalActionCompletedStatus
} from "../actions/goalActions";
import {BASE_API} from "../api/api";

export const goalMiddleware = ({ dispatch, getState }) => next => action => {
    next(action)

    if(action.type === types.ACTION_GOAL_FETCH) {
        const userId = getState().auth.userId

        fetch(BASE_API + "/api/v1/goal/user/" + userId)
            .then(r => r.json())
            .then(goals => {
                if(goals.length) {
                    dispatch(goalActionSet(goals))
                    const goal = goals.filter(goal => goal.completed !== true)
                    if (!isNaN(goal[0].id)) {
                        dispatch(goalWorkoutActionFetch(goal[0].workouts))
                        dispatch(goalActionTrue(goal[0].id))
                    }
                }
            })
            .catch(error => {
                dispatch(goalActionError("ACTION_GOAL_FETCH: " + error.message))
            })
    }

    if(action.type === types.ACTION_GOAL_NEW) {
        const startDate = "" + getState().goals.newGoalDate.startDate
        const userId = getState().auth.userId

        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body: JSON.stringify({
                "startDate": startDate,
                "completed": false,
                "difficulty": 0,
                "accountId": userId
            })
        };

        fetch(BASE_API + "/api/v1/goal", options)
            .then(r => r.json())
            .then(goal => {
                dispatch(goalActionAdd(goal))
            })
            .catch(error => {
                dispatch(goalActionError("ACTION_GOAL_NEW: " + error.message))

            })
    }

    if(action.type === types.ACTION_GOAL_COMPLETED) {
        const updatedGoal = {
            "id": getState().goals.currentGoal.id,
            "title": getState().goals.currentGoal.title,
            "startDate": getState().goals.currentGoal.startDate,
            "completed": true,
            "difficulty": getState().goals.currentGoal.difficulty,
            "accountId": getState().goals.currentGoal.accountId
        }
        const options = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body: JSON.stringify(updatedGoal)
        };

        fetch(BASE_API + "/api/v1/goal/" + getState().goals.goalId, options)
            .then(r => {
                dispatch(goalActionChangeList(updatedGoal))
                dispatch(goalActionClear())
            })
            .catch(error => {
                dispatch(goalActionError( "ACTION_GOAL_COMPLETED: " + error.message))
            })
    }

    if(action.type === types.ACTION_GOAL_CALC_PROGRESS) {
        console.log("GOAL MIDDLEWARE - Progress " + (getState().goals.goalWorkouts.length) + " - " + getState().goals.completedWorkouts.length)
        const completedWorkoutsNum = (getState().goals.completedWorkouts.length)
        const allWorkouts = getState().goals.goalWorkouts.length
        let progress = (completedWorkoutsNum / allWorkouts) * 100

        if(allWorkouts !== 0 ){
            dispatch(goalActionSetProgress(progress))
        }
    }

    if(action.type === types.ACTION_GOAL_DELETE) {
        fetch(BASE_API + "/api/v1/goal/" + getState().goals.goalId + "/goalAndWorkout", {method: 'DELETE'})
            .then(r => {
                dispatch(goalActionClear())
            })
            .catch(error => {
                dispatch(goalActionError(error.message))
            })
    }

    if(action.type === types.ACTION_GOAL_WORKOUT_FETCH) {
        const sets = []
        const workouts = []
        for (let i = 0; i < action.payload.length; i++) {
            getState().workouts.workouts.map(workout => {
                if(workout.id === action.payload[i]) {
                    workouts.push(workout)
                    sets.push(workout.sets)
                }
            })
        }
        dispatch(goalWorkoutActionSet(workouts))
    }

    if(action.type === types.ACTION_GOAL_WORKOUT_FETCH_NEW) {

        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body: JSON.stringify(action.payload
            )
        };
        fetch(BASE_API + "/api/v1/workout/getMultiple", options)
            .then(r => r.json())
            .then(workouts => {
                dispatch(goalWorkoutActionSet(workouts))
            })
            .catch(error => {
                dispatch(goalWorkoutActionError("ACTION_GOAL_WORKOUT_FETCH_NEW: " + error.message))
            })
    }

    if(action.type === types.ACTION_GOAL_WORKOUT_COMPLETE) {

        const updatedWorkout = {
            "id": action.payload.id,
            "title": action.payload.title,
            "type": action.payload.type,
            "difficulty": action.payload.difficulty,
            "madeByContributor": action.payload.madeByContributor,
            "sets": action.payload.sets,
            "completed": 1
        }
        const options = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body: JSON.stringify(updatedWorkout)
        };

        fetch(BASE_API + "/api/v1/workout/" + action.payload.id, options)
            .then(r => {
                dispatch(goalWorkoutActionCompletedList(action.payload))
                dispatch(goalWorkoutActionChangeList(updatedWorkout))
                dispatch(goalActionCalcProgress())
            })
            .catch(error => {
                dispatch(goalWorkoutActionError("ACTION_GOAL_WORKOUT_COMPLETE: " + error.message))
            })
    }

    if(action.type === types.ACTION_GOAL_WORKOUT_COMPLETED_LIST) {
        console.log("GOAL MIDDLEWARE - completed workout list " + (getState().goals.goalWorkouts.length+1) + " - " + getState().goals.completedWorkouts.length)
        if((getState().goals.goalWorkouts.length+1) === getState().goals.completedWorkouts.length){
            dispatch(goalActionCompletedStatus())
        }
    }

    if(action.type === types.ACTION_GOAL_CALC_PROGRESS) {
        const completedWorkoutsNum = (getState().goals.completedWorkouts.length)
        const allWorkouts = getState().goals.goalWorkouts.length
        let progress = (completedWorkoutsNum / allWorkouts) * 100
        console.log("GOAL MIDDLEWARE - progress: " + progress)
    }

    if(action.type === types.ACTION_GOAL_EXERCISE_FETCH) {
        const exercises = []
        for (let i = 0; i < action.payload.length; i++) {
            getState().exercises.exercises.map(x => {
                if(x.id === action.payload[i]) {
                    exercises.push(x)
                }
            })
        }
        dispatch(goalExerciseActionSet(exercises))
    }

/*    if(action.type === types.ACTION_GOAL_WORKOUT_FETCH_NEW) {
        console.log("GOAL MIDDLEWARE - workout fetch new " + action.payload)

        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body: JSON.stringify(action.payload
            )
        };
        fetch(BASE_API + "/api/v1/workout/getMultiple", options)
            .then(r => r.json())
            .then(workouts => {
                console.log("GOAL MIDDLEWARE - workouts: " + JSON.stringify(workouts))
                dispatch(goalWorkoutActionSet(workouts))
            })
            .catch(error => {
                dispatch(goalWorkoutActionError("ACTION_GOAL_WORKOUT_FETCH_NEW: " + error.message))
            })
    }*/
}