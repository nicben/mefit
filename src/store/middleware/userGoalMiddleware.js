import * as types from '../constants/ActionTypes'
import {BASE_API} from "../api/api";
import {goalActionFetch, goalWorkoutActionFetchNew, goalWorkoutActionSet} from "../actions/goalActions";
//import {goalWorkoutActionSet} from "../actions/goalActions";
import {
    userCreateGoalActionError,
    userCreateWorkoutActionError,
    userGoalActionNew,
    userSetActionError,
    userWorkoutActionNew
} from "../actions/userGoalsActions";

export const userGoalMiddleware = ({ dispatch, getState }) => next => action => {
    next(action)

    if(action.type === types.ACTION_USER_CREATE_WORKOUT){
        const payload = action.payload;
        console.log("Dette er PAYLOAD: "+payload)
        const workouts = []
        const currentWorkouts = []

        for(let i = 0; i < payload.length; i++){
            let newWorkout = {}
            if(!payload[i].length){
                newWorkout = {}
                workouts.push(newWorkout)
            }
            else{
                newWorkout = {
                    title: `Day${i+1}`,
                    type: action.listOfTypes[i],
                    difficulty: 1,
                    madeByContributor: false,
                    sets: payload[i],
                    completed: 0
                }
                workouts.push(newWorkout)
                currentWorkouts.push(newWorkout)
            }
        }

        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body: JSON.stringify(workouts)
        };

        console.log("Dette er workouts: "+JSON.stringify(workouts))
        fetch(BASE_API + "/api/v1/workout/Multiple", options)
            .then(r => r.json())
            .then(newGoal => {
                console.log("DETTE ER INNE I FETCHEN - new goal: "+newGoal)
                dispatch(userGoalActionNew(newGoal))
            })
            .catch(error => {
                dispatch(userCreateWorkoutActionError(error.message))
            })
    }

    if(action.type === types.ACTION_USER_CREATE_GOAL) {
        const payload = action.payload;
        const goalId = getState().goals.goalId;

        let workouts = payload.filter((workout) => workout !== 0)

        const options = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body: JSON.stringify(workouts)
        };
        fetch(BASE_API + `/api/v1/goal/${goalId}/workout`, options)
            .then(r => {
                console.log("USER GOAL MIDDLEWARE - user create goal - goalWorkoutActionNew")
                dispatch(goalWorkoutActionFetchNew(workouts))
            })
            .catch(error => {
                dispatch(userCreateGoalActionError(error.message))
            })
    }

    if(action.type === types.ACTION_USER_CREATE_SET) {
        const payload = action.payload
        const listOfTypes = action.listOfTypes

        let workouts = []
        for(const [key, value] of Object.entries(payload)){
            workouts.push(value)
        }

        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body: JSON.stringify(workouts)
        };
        fetch(BASE_API + "/api/v1/set/addMultiple2", options)
            .then(r => r.json())
            .then(set => {
                dispatch(userWorkoutActionNew(set, listOfTypes))
            })
            .catch(error => {
                dispatch(userSetActionError(error.message))
            })
    }

    if(action.type === types.ACTION_USER_ADD_GOAL_TITLE) {
        const goalId = getState().goals.goalId;
        const goalDetails = {
            id: goalId,
            title: action.title,
            startDate: getState().goals.currentGoal.startDate,
            completed: false,
            difficulty: 0,
            accountId: getState().auth.userId
        }

        const options = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body: JSON.stringify(goalDetails)
        };
        fetch(`${BASE_API}/api/v1/goal/${goalId}`, options)
            .then(r => {
                dispatch(goalActionFetch())
            })
            .then(goal => console.log("Dette er inne i fetchen! Den returnerer: "+JSON.stringify(goal)))
            .catch(error => {
                dispatch(userSetActionError(error.message))
            })
    }
}