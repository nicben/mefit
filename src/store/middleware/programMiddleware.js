import * as types from '../constants/ActionTypes'
import {programActionFetch, programActionSet} from "../actions/programActions";
import {BASE_API} from "../api/api";
import {loginRefresh} from "../actions/authActions";
import {ACTION_PROGRAM_UPDATE} from "../constants/ActionTypes";

export const programMiddleware = ({dispatch, getState}) => next => action => {
    next(action)

    if (action.type === types.ACTION_PROGRAM_FETCH) {
        fetch(BASE_API + "/api/v1/program")
            .then(response => response.json())
            .then(r => dispatch(programActionSet(r)))
            .catch(error => {
                console.log("Error in program fetch: "+error)
            })
    }

    if (action.type === types.ACTION_PROGRAM_CREATE) {

        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token': getState().auth.userToken
            },
            body: JSON.stringify(action.payload)
        };

        fetch(BASE_API + "/api/v1/program/Multiple", options)
            .then(response => response.json())
            .then(r => dispatch(loginRefresh(getState().auth.userToken)))
            .catch(error =>
                console.log("error " + JSON.stringify(error))
            )
    }

    if (action.type === types.ACTION_PROGRAM_UPDATE) {
        const payload = action.payload;
        const workoutId = payload.workoutIds;
        const programId = payload.programId;
        const options = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token': getState().auth.userToken
            },
            body: JSON.stringify(workoutId)
        };
        fetch(`${BASE_API}/api/v1/program/${programId}/workout`, options)
            .then(response => response.json())
            .catch(error => console.log(JSON.stringify(error)));

    }
}