import * as types from '../constants/ActionTypes'
import {loginRefresh, setUser} from "../actions/authActions";
import {BASE_API} from "../api/api";



export const accountMiddleware = ({ dispatch, getState } ) => next => action => {
    next(action);
    const payload = action.payload;


    if(action.type === types.ACTION_CREATE_ACCOUNT){
        let height = payload.inputHeight;
        let weight = payload.inputWeight;
        let contributeStatus = payload.contributorStatus;
        let firstname = payload.inputFirstname;
        let lastname = payload.inputLastname;
        let email = payload.inputEmail;



        if(payload.inputHeight===""|| payload.inputHeight==0)height = getState().auth.height;
        if(payload.inputWeight===""|| payload.inputWeight==0) weight = getState().auth.weight
        if(payload.contributorStatus===""|| payload.contributorStatus==0) contributeStatus = getState().auth.contributor;
        if(payload.inputFirstname===""|| payload.inputFirstname==0)firstname = getState().auth.firstName;
        if(payload.inputLastname===""|| payload.inputLastname==0) lastname = getState().auth.lastName;
        if(payload.inputEmail==="") email = getState().auth.email;



        const updatedUser = {
            "fitnessLevel": 0,
            "imageUrl": getState().auth.imageUrl,
            "height":height,
            "weight": weight,
            "admin": 0,
            "contributer": contributeStatus,
            "firstName":firstname,
            "lastName": lastname,
            "email": email,
            "googleId": getState().auth.googleID
        }

        console.log("USERTOKEN " + getState().auth.userToken)
        const updatedUser2 = {
            "id":getState().auth.userId,
            "fitnessLevel": 0,
            "imageUrl": getState().auth.imageUrl,
            "height": height,
            "weight": weight,
            "admin": 0,
            "contributer": contributeStatus,
            "firstName": firstname,
            "lastName": lastname,
            "email": email,
            "googleId": getState().auth.googleID,
        }
        console.log(JSON.stringify(updatedUser2))
        const body = {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body:JSON.stringify(updatedUser)
        }
        //fetch(config.BASE_URL_DEV+"/api/v1/account/update",body)
        fetch(BASE_API +"/api/v1/account/"+getState().auth.userId,body)
            .then(response => {
                console.log("Dette er i Account middleware")
                dispatch(setUser({account:updatedUser2,token:getState().auth.userToken}))
            });
    }

}