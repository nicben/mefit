import * as types from '../constants/ActionTypes'
import api from "../api/storage";
import {workoutActionSet, workoutActionDelete, updateWorkout} from "../actions/workoutActions";
import {BASE_API} from "../api/api";
import {ACTION_WORKOUT_CREATE_SET_WORKOUT, ACTION_WORKOUT_UPDATE} from "../constants/ActionTypes";
import {loginRefresh} from "../actions/authActions";

export const workoutMiddleware = ({ dispatch, getState }) => next => action => {
    next(action)

    if(action.type === types.ACTION_WORKOUT_FETCH) {
        fetch(BASE_API + "/api/v1/workout")
            .then(response => response.json())
            .then(r => {
                dispatch(workoutActionSet(r))
            })
    }

    if(action.type === types.ACTION_WORKOUT_DELETE){
        api.getWorkouts(workout => {
            dispatch(workoutActionDelete(workout))
        })
    }

    if(action.type === types.ACTION_WORKOUT_ERROR) {

    }
    if(action.type === types.ACTION_WORKOUT_CREATE_SET_WORKOUT ){
        console.log("SETTENE :) "+JSON.stringify(action.payload.notCreatedSets) )

        const body = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body:JSON.stringify(action.payload.notCreatedSets)
        }
        console.log("Workout ID " + action.payload.workoutId)
        fetch(BASE_API+"/api/v1/set/addMultiple",body)
            .then(response => response.json())
            .then(r => {
                let list = JSON.parse(JSON.stringify(action.payload.oldSets))
                const allSets = list.concat(r);
                dispatch(updateWorkout({id: action.payload.workoutId,sets: allSets}))
                dispatch(loginRefresh(getState().auth.userToken))
            })
    }

    if(action.type === types.ACTION_WORKOUT_UPDATE ){
        const body = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body:JSON.stringify(action.payload.sets)
        }

        console.log("Workout ID2 " +
        JSON.stringify(body))

        fetch(BASE_API+"/api/v1/workout/"+action.payload.id+"/set",body)
            .then(response => response.json())
            .then(r => {
                console.log("THIS WE MADE " +JSON.stringify(r));
            }).catch(error => console.log(JSON.stringify(error)))


    }
}