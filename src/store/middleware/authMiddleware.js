import * as types from '../constants/ActionTypes'
import {setUser} from "../actions/authActions";
import {BASE_API} from "../api/api";
import {setErrorAccount} from "../actions/accountActions";
import {getSets, overviewActionFetchExercises} from "../actions/overviewActions";
import {workoutActionFetch} from "../actions/workoutActions";
import {programActionFetch} from "../actions/programActions";
import {goalActionFetch, goalActionProgress} from "../actions/goalActions";
import {adminActionFetchContributors, adminActionFetchRequests} from "../actions/adminActions";

export const authMiddleware = ({ dispatch } ) => next => action => {
    const payload = action.payload;
    next(action);

    if(action.type === types.LOGIN) {
        const tokenBlob = new Blob([JSON.stringify({ tokenId: payload.tokenId,googleId:payload.googleId }, null, 2)], { type: 'application/json' });
        const options = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            method: 'POST',
            body: tokenBlob,
        };

        fetch(BASE_API + "/api/Auth/login", options)
            .then(r => {
                r.json().then(user => {
                    localStorage.setItem("meFitLoginToken", JSON.stringify(user.token));
                    dispatch(setUser(user));
                    dispatch(overviewActionFetchExercises())
                    dispatch(workoutActionFetch())
                    dispatch(getSets())
                    dispatch(goalActionFetch())
                    dispatch(programActionFetch())
                });
            }).catch(error => {
                localStorage.clear();
                dispatch(setErrorAccount(error))
        })
    }
    else if(action.type === types.LOGIN_REFRESH){
        const body = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : payload
            },
        }
        fetch(BASE_API + "/api/v1/account",body)
            .then(response => {
                response.json().then(user => {
                    dispatch(setUser({account: user,"token":payload}));
                    dispatch(getSets())
                    dispatch(programActionFetch())
                    dispatch(overviewActionFetchExercises())
                    dispatch(workoutActionFetch())
                    dispatch(adminActionFetchContributors())
                    dispatch(adminActionFetchRequests())
                    dispatch(goalActionFetch())

                })
            }).catch(error => {
            localStorage.clear()
            dispatch(setErrorAccount(error))
        })
    }
}

