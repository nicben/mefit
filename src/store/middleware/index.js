import {applyMiddleware} from "redux";
import {authMiddleware} from "./authMiddleware";
import {sessionMiddleware} from "./sessionMiddleware";
import {goalMiddleware} from "./goalMiddleware";
import {workoutMiddleware} from "./workoutMiddleware";
import {programMiddleware} from "./programMiddleware";
import {exerciseMiddleware} from "./exerciseMiddleware";
import {accountMiddleware} from "./accountMiddleware";
import {adminMiddleware} from "./adminMiddleware";
import {overviewMiddleware} from "./overviewMiddleware";
import {userGoalMiddleware} from "./userGoalMiddleware";
import {GoalProgramMiddleware} from "./goalProgramMiddleware";

export default applyMiddleware(
    adminMiddleware,
    authMiddleware,
    sessionMiddleware,
    goalMiddleware,
    workoutMiddleware,
    exerciseMiddleware,
    userGoalMiddleware,
    accountMiddleware,
    overviewMiddleware,
    programMiddleware,
    GoalProgramMiddleware
   // exerciseMiddleware
)

