import * as types from '../constants/ActionTypes'
import {BASE_API} from "../api/api";

import {userCreateGoalActionError} from "../actions/userGoalsActions";
import {CREATE_GOALS_GOAL_PROGRAMGOALS} from "../constants/ActionTypes";
import {createEmptyGoal, fillEmptyGoal, goalActionFetch} from "../actions/goalActions";
import {loginRefresh} from "../actions/authActions";

export const GoalProgramMiddleware = ({ dispatch,getState }) => next => action => {
    next(action)
    const payload = action.payload;
    if(action.type === types.CREATE_GOALS_FROM_PROGRAM){

        const workouts = payload.workouts;

        const body = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body:JSON.stringify(workouts)
        }

        fetch(BASE_API + `/api/v1/workout/Multiple`, body)
            .then(r => r.json())
            .then(response =>{
                const createdWorkoutsId = response.filter((id) => id!== 0)
                payload["createdWorkoutsId"] = createdWorkoutsId;
                dispatch(createEmptyGoal(payload));
            })
            .catch(error => {
                console.log(JSON.stringify(error))
            })
    }

    if(action.type === types.CREATE_GOALS_GOAL_PROGRAMGOALS){
        const date = action.payload.date;
        const title = action.payload.title;
        const body = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body:JSON.stringify({
                "title":title,
                "startDate":date,
                "completed":false,
                "difficulty": 0,
                "accountId": getState().auth.userId
            })
        }


        fetch(BASE_API+"/api/v1/goal",body)
            .then(response => response.json())
            .then(r =>{
                console.log("yoyo")
                payload["goalId"] = r.id;
                dispatch(fillEmptyGoal(payload));
            } )
    }

    if(action.type === types.FILL_GOAL_PROGRAMGOALS){
        const workoutId = payload.createdWorkoutsId;
        const goalId = payload.goalId;
        const body = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body:JSON.stringify(workoutId)
        }
        fetch(BASE_API+"/api/v1/goal/"+goalId+"/workout",body)
            .then(response => dispatch(loginRefresh(getState().auth.userToken)))

    }
}
