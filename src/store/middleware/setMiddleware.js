import * as types from '../constants/ActionTypes';
import {BASE_API} from "../api/api";
import { setActionError} from "../actions/setActions";
import { userWorkoutActionNew} from "../actions/userGoalsActions";

export const setMiddleware = ({ dispatch, getState }) => next => action => {
    next(action)

    if(action.type === types.ACTION_SET_CREATE) {
        const payload = action.payload
        let workouts = []
        for(const [, value] of Object.entries(payload)){
            workouts.push(value)
        }

        const options = {
         method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'token' : getState().auth.userToken
            },
            body: JSON.stringify(workouts)
        };
        fetch(BASE_API + "/api/v1/set/addMultiple2", options)
            .then(r => r.json())
            .then(set => {
                dispatch(userWorkoutActionNew(set))
            })
            .catch(error => {
                console.log("Error in set creation: "+error)
            })
    }
    /*
    if(action.type === types.ACTION_GOAL_DELETE) {
        fetch(BASE_API + "/api/v1/goals/" + getState().goals.goalId, {method: 'DELETE'})
            //fetch(goalApi + '/' + getState().goals.goalId, {method: 'DELETE'})
            .then(r => console.log("Delete respons " + r))
            .catch(error => {
                dispatch(goalActionError(error.message))
            })
    }*/
}